       IDENTIFICATION DIVISION.
       PROGRAM-ID.    WGA022I.
      ******************************************************************
      *    TITLE ........... INQUIRE/AUTHORIZE ACCOUNT INDEX FROM WEB
      *                     (INQAINDX)
      *    LANGUAGE ........ COBOL Z/OS, DB2, CICS
      *    AUTHOR .......... H. VANDERWEIT
      *    DATE-WRITTEN .... JULY, 2009
      *
      *                      DESCRIPTION
      *                      -----------
      *    AS REQUESTED FROM THE WEBSPHERE CLIENT APPLICATION, THIS
      *    PROGRAM PROCESSES ACCOUNT INDEX AUTHORIZATION AND INQUIRY
      *    FUNCTIONS.
      *
      *    THIS PROGRAM RECEIVES A COMMAREA FROM THE WEBSPHERE CLIENT,
      *    PERFORMS ONE OF THE FOLLOWING REQUESTED FUNCTIONS, AND
      *    RETURNS THE RETURN-STATUS-CODE AND UP TO FOUR MESSAGES
      *    BACK TO THE CLIENT:
      *
      *    1) AUTHONLY - AUTHORIZE ACCOUNT INDEX ACCESS
      *    2) INQAUTH  - AUTHORIZE AND RETRIEVE ACCOUNT INDEX INFO
      *    3) INQONLY  - RETRIEVE ACCOUNT INDEX INFO ONLY
      *
      *    DB2 TABLE(S)      HOW ACCESSED
      *    ------------------------------------------------
      *    ATI_ACTI_V        SELECT
      *    ACI_ACTI_EFCTV_V  SELECT
      *    MSL_MSG_LINE_V    SELECT
      *    YED_CNT_EXCD_V    SELECT
      *    YUS_SEC_USR_V     SELECT
      *    YTS_UNT_PMS_V     SELECT
      *
      ******************************************************************
      *                      MAINTENANCE HISTORY                       *
      ******************************************************************
      *    MODIFIED BY .....  DEVMLJ
      *    DATE MODIFIED ...  02/16/2010
      *    MODIFICATION ....  USE THE FIRST START DATE FOR AN INDEX
      ******************************************************************
      ******************************************************************
002MLJ*    MODIFIED BY .....  DEVMLJ                   FP3660
      *    DATE MODIFIED ...  08/25/2011
      *    MODIFICATION ....  USE THE MAX START DATE FOR AN INDEX
      ******************************************************************
      ******************************************************************
003MLJ*    MODIFIED BY .....  DEVMLJ                   FP6171
      *    DATE MODIFIED ...  07/15/2014
      *    MODIFICATION ....  MUST HAVE ACCESS FOR BOTH INDEXCPY, AND
      *                       INDEXTBL
      ******************************************************************
      /
       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
       01  FILLER                      PIC X(36)       VALUE
           'WGA022I WORKING-STORAGE BEGINS HERE'.

       01  WS-MISCELLANEOUS.
           05  WS-SQLCOUNT             PIC S9(8) COMP  VALUE ZEROES.
           05  WS-CURR-DATE-ISO        PIC X(10)       VALUE ZEROES.
           05  WS-START-DT-ISO         PIC X(10)       VALUE SPACES.
           05  WS-HOLD-VALUE           PIC X(08)       VALUE SPACES.

       01  DB2-HOST-VARIABLES.
           05  HV-ANY-DATE             PIC X(10)       VALUE SPACES.
           05  HV-ISO-DATE             PIC X(10)       VALUE SPACES.
           05  HV-ACCT-INDX-CD         PIC X(08)       VALUE SPACES.

       01  COMMON-IFIS-FIELDS-SY00.
      *    *************************************************************
      *    *   FIELD NAMES RETAINED TO SIMPLIFY CUT/PASTING OF
      *    *   EDITING LOGIC FROM IFIS/FORECROSS PROGRAMS.
      *    *************************************************************
           05  MSG-QTY-SY00            PIC S9(4) COMP  SYNC
                                                       VALUE ZEROES.
           05  MSG-ID-SY00             PIC 9(06)       VALUE ZEROES.
           05  MAP-ELMNT-NAME-SY00     PIC X(30)       VALUE SPACES.
      /
      ******************************************************************
      *01  INQAINDX-PARM-AREA.
      ******************************************************************
           COPY WGA022I.
      /
       01  WSLG-USING-LIST.
           05  WSLG-CONSOLE-MSG-IND    PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-ID             PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-DATE           PIC X(10)       VALUE SPACES.
           05  WSLG-MSG-TIME           PIC X(08)       VALUE SPACES.
           05  WSLG-PROGRAM-NAME       PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-SEQ-NUM        PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-TEXT           PIC X(80)       VALUE SPACES.

       01  WSLG-DIAGNOSTIC-AREA.
           05  FILLER                  PIC X(05)       VALUE 'PARA:'.
           05  WSLG-PARAGRAPH          PIC X(30)       VALUE SPACES.
           05  FILLER                  PIC X(05)       VALUE 'TASK:'.
           05  WSLG-CICS-TASK-NUMBER   PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  WSLG-DIAGNOSTIC-INFO    PIC X(30)       VALUE SPACES.

       01  WSLG-MISC-AREA.
           05  WSLG-PROGRAM-UIS000W    PIC X(08)       VALUE 'UIS000W '.
           05  WSLG-CICS-RESP          PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-CICS-RESP2         PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-EIBCALEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-CHANLLEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-DB2-STATEMENT      PIC X(08)       VALUE SPACES.
           05  WSLG-DB2-TABLE          PIC X(16)       VALUE SPACES.
           05  WSLG-SQL-NBR            PIC 9(04)       VALUE ZEROES.
           05  WSLG-SQL-CODE           PIC +++++9      VALUE ZEROES.

       01  WSLG-MESSAGE-AREA.
           05  WSLG-SUCCESSFUL-MSG-ID  PIC X(08)       VALUE 'WFGA000I'.
           05  WSLG-SUCCESSFUL-MSG-TEXT
                                       PIC X(40)       VALUE
               'The request was successfully completed. '.

           05  WSLG-CHNL-ID-MSG-ID     PIC X(08)       VALUE 'WFGA001E'.
           05  WSLG-CHNL-ID-MSG-TEXT   PIC X(80)       VALUE
               'The received channel ID value is unrecognized.'.

           05  WSLG-CHNL-VERS-MSG-ID   PIC X(08)       VALUE 'WFGA002E'.
           05  WSLG-CHNL-VERS-MSG-TEXT PIC X(80)       VALUE
               'The received channel version value is unrecognized.'.

           05  WSLG-CHNL-LGTH-MSG-ID   PIC X(08)       VALUE 'WFGA003E'.
           05  WSLG-CHNL-LGTH-MSG-TEXT PIC X(80)       VALUE
               'The received channel length is not equal to the defined
      -        'channel version length. '.

           05  WSLG-FATAL-DB2-MSG-ID   PIC X(08)       VALUE 'WFGA004S'.
           05  WSLG-FATAL-DB2-MSG-TEXT.
               10  FILLER              PIC X(49)       VALUE
                   'DB2 returned a severe SQL error on an attempt to '.
               10  WSLG-FATAL-DB2-MSG-FIELDS.
                   15  WSLG-FATAL-DB2-STATEMENT
                                       PIC X(08)       VALUE SPACES.
                   15  FILLER          PIC X(06)       VALUE 'table '.
                   15  WSLG-FATAL-DB2-TABLE
                                       PIC X(16)       VALUE SPACES.
      /
      ******************************************************************
      *    SQL COMMAREA
      ******************************************************************
           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
      ******************************************************************
      *    ACI_ACTI_EFCTV_V
      ******************************************************************
       01  ACI-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'ACI*'.
           05  ACI-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVACI
           END-EXEC.
      /
      ******************************************************************
      *    ATI_ACTI_V
      ******************************************************************
       01  ATI-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'ATI*'.
           05  ATI-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVATI
           END-EXEC.
      /
      ******************************************************************
      *    MSL_MSG_LINE_V
      ******************************************************************
       01  MSL-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'MSL*'.
           05  MSL-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVMSL
           END-EXEC.
      /
      ******************************************************************
      *    YED_CNT_EXCD_V
      ******************************************************************
       01  YED-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'YED*'.
           05  YED-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVYED
           END-EXEC.
      /
      ******************************************************************
      *    YTS_UNT_PMS_V
      ******************************************************************
       01  YTS-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'YTS*'.
           05  YTS-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVYTS
           END-EXEC.
      /
      ******************************************************************
      *    YUS_SEC_USR_V
      ******************************************************************
       01  YUS-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'YUS*'.
           05  YUS-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVYUS
           END-EXEC.
      /
       LINKAGE SECTION.
      /
       01  DFHCOMMAREA.
           05  COMM-CHANNEL-HEADER.
               10  COMM-CHANNEL-ID     PIC X(08).
                   88  COMM-CHANNEL-ID-OK              VALUE 'INQAINDX'.
               10  COMM-VERS-RESP-CD   PIC X(08).
                   88  COMM-VERSION-01-00-00           VALUE '01.00.00'.
           05  COMM-INQAINDX-INPUT-PARMS
                                       PIC X(500).

       PROCEDURE DIVISION.

      /
       A0000-MAINLINE-ROUTINE.
      ******************************************************************
      *    PERFORM ROUTINE TO EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      *    DEPENDING ON THE REQUESTED FUNCTION, PERFORM THE
      *    A1000-AUTHORIZATION-ROUTINE AND/OR I1000-INQUIRY-ROUTINE.
      ******************************************************************

           MOVE 'A0000-MAINLINE-ROUTINE'
                                       TO WSLG-PARAGRAPH.

           PERFORM A0500-EDIT-COMMAREA-VERSION.

           MOVE DFHCOMMAREA            TO INQAINDX-PARM-AREA.
           INITIALIZE                     INQAINDX-OUTPUT-PARMS.
           INITIALIZE                     INQAINDX-RETURN-STATUS-PARMS.

           INSPECT INQAINDX-USER-CD CONVERTING
                                'abcdefghijklmnopqrstuvwxyz' TO
                                'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           EVALUATE INQAINDX-FUNCTION

             WHEN 'AUTHONLY'
               PERFORM A1000-AUTHORIZATION-ROUTINE

             WHEN 'INQONLY'
               PERFORM I1000-INQUIRY-ROUTINE

             WHEN 'INQAUTH'
               PERFORM A1000-AUTHORIZATION-ROUTINE
               PERFORM I1000-INQUIRY-ROUTINE

             WHEN OTHER
               MOVE 'A00001'           TO INQAINDX-RETURN-MSG-NUM
               MOVE 'Function'         TO INQAINDX-RETURN-MSG-FIELD
               MOVE 'The requested function is invalid       '
                                       TO INQAINDX-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-EVALUATE.

           GO TO Z9999-RETURN-SUCCESSFUL.
      /
       A0500-EDIT-COMMAREA-VERSION.
      ******************************************************************
      *    EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      ******************************************************************

           MOVE 'A0500-EDIT-COMMAREA-VERSION'
                                       TO WSLG-PARAGRAPH.
           INSPECT COMM-CHANNEL-ID
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           INITIALIZE                     INQAINDX-PARM-AREA.
           MOVE COMM-CHANNEL-HEADER    TO INQAINDX-CHANNEL-HEADER.

           IF COMM-CHANNEL-ID-OK
               EVALUATE TRUE
               WHEN COMM-VERSION-01-00-00

                   IF EIBCALEN = LENGTH OF INQAINDX-PARM-AREA
                       NEXT SENTENCE
                   ELSE
                       SET INQAINDX-RESPONSE-ERROR
                                       TO TRUE
                       MOVE 'E'        TO INQAINDX-RETURN-STATUS-CODE
                       MOVE  1         TO INQAINDX-RETURN-MSG-COUNT
                       MOVE 'A05001'   TO INQAINDX-RETURN-MSG-NUM
                       MOVE 'CHANNEL-ID'
                                       TO INQAINDX-RETURN-MSG-FIELD
                       MOVE 'The channel length is invalid           '
                                       TO INQAINDX-RETURN-MSG-TEXT
                       MOVE WSLG-CHNL-LGTH-MSG-ID
                                       TO WSLG-MSG-ID
                       MOVE WSLG-CHNL-LGTH-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                       MOVE 'N'        TO WSLG-CONSOLE-MSG-IND
                       MOVE SPACES     TO WSLG-DIAGNOSTIC-INFO
                       MOVE EIBCALEN   TO WSLG-EIBCALEN-NUMEDIT
                       MOVE LENGTH OF INQAINDX-PARM-AREA
                                       TO WSLG-CHANLLEN-NUMEDIT
                       STRING 'EIBCALEN '     WSLG-EIBCALEN-NUMEDIT
                              ' VERSUS '      WSLG-CHANLLEN-NUMEDIT
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                       PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                       GO TO Z9999-RETURN-TO-CALLING-PGM
                   END-IF

               WHEN OTHER
                   SET INQAINDX-RESPONSE-ERROR
                                       TO TRUE
                   MOVE 'E'            TO INQAINDX-RETURN-STATUS-CODE   E
                   MOVE  1             TO INQAINDX-RETURN-MSG-COUNT
                   MOVE 'A05002'       TO INQAINDX-RETURN-MSG-NUM
                   MOVE 'CHANNEL-ID'   TO INQAINDX-RETURN-MSG-FIELD
                   MOVE 'The channel version is invalid          '
                                       TO INQAINDX-RETURN-MSG-TEXT
                   MOVE WSLG-CHNL-VERS-MSG-ID
                                       TO WSLG-MSG-ID
                   MOVE WSLG-CHNL-VERS-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                   MOVE 'N'            TO WSLG-CONSOLE-MSG-IND
                   MOVE SPACES         TO WSLG-DIAGNOSTIC-INFO
                   STRING 'Received version: ' COMM-VERS-RESP-CD
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                   PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                   GO TO Z9999-RETURN-TO-CALLING-PGM
               END-EVALUATE
           ELSE
               SET INQAINDX-RESPONSE-ERROR
                                       TO TRUE
               MOVE 'E'                TO INQAINDX-RETURN-STATUS-CODE
               MOVE  1                 TO INQAINDX-RETURN-MSG-COUNT
               MOVE 'A05003'           TO INQAINDX-RETURN-MSG-NUM
               MOVE 'Channel-Id'       TO INQAINDX-RETURN-MSG-FIELD
               MOVE 'The channel id value is invalid         '
                                       TO INQAINDX-RETURN-MSG-TEXT
               MOVE WSLG-CHNL-ID-MSG-ID
                                       TO WSLG-MSG-ID
               MOVE WSLG-CHNL-ID-MSG-TEXT
                                       TO WSLG-MSG-TEXT
               MOVE 'N'                TO WSLG-CONSOLE-MSG-IND
               MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
               STRING 'Received Channel Id: ' COMM-CHANNEL-ID
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
               PERFORM Z9998-LOG-ERROR-VIA-UIS000W
               GO TO Z9999-RETURN-TO-CALLING-PGM
           END-IF.
      /
       A1000-AUTHORIZATION-ROUTINE.
      ******************************************************************
      *    PERFORM SQL ROUTINE TO RETRIEVE CONTENT SECURITY PERMISSIONS
      *    FOR A USER.  VERIFY THE USER HAS ADD PERMISSION FOR AN INDEX
      *    IN THE RANGE SPECIFIED IN THE ROW.
      *    IF NO CONTENT SECURITY PERMISSION, VERIFY USER HAS PERMISSION
      *    VIA A SECURITY TEMPLATE.
      ******************************************************************

           MOVE 'A1000-AUTHORIZATION-ROUTINE'
                                       TO WSLG-PARAGRAPH.

           MOVE INQAINDX-USER-CD       TO YED-FK-YEH-SCR-USER-CD.
           MOVE INQAINDX-ACCT-INDX-CD  TO HV-ACCT-INDX-CD.
           MOVE +0                     TO WS-SQLCOUNT.
           PERFORM S1200-SELECT-YED.

           IF WS-SQLCOUNT = +0
               MOVE INQAINDX-USER-CD   TO YUS-SCRTY-USER-CD
               PERFORM S1250-SELECT-YTS
           END-IF.

      *   MUST HAVE ADD PERMISSION FOR BOTH INDEXCPY AND INDEXTBL

003MLJ     IF WS-SQLCOUNT = +0
               MOVE +0                 TO WS-SQLCOUNT
               MOVE INQAINDX-USER-CD   TO YUS-SCRTY-USER-CD
               PERFORM S1255-SELECT-YTS-INDEXTBL
           END-IF.

           IF WS-SQLCOUNT = +0
               MOVE 'A1001'            TO INQAINDX-RETURN-MSG-NUM
               MOVE 'USER-CD/ACCT-INDX-CD'
                                       TO INQAINDX-RETURN-MSG-FIELD
               MOVE 'NO AUTHORIZATION' TO INQAINDX-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.
      /
       I1000-INQUIRY-ROUTINE.
      ******************************************************************
      *    PERFORM SQL ROUTINE TO GET THE CURRENT DATE.
      *    IF BLANK, DEFAULT THE PASSED START-DT TO THE CURRENT DATE.
      *    OTHERWISE, EDIT/RETRIEVE START-DT IN ISO FORMAT (YYYY-MM-DD)
      *               AND ENSURE THAT IT IS NOT A FUTURE DATE.
      *    RETRIEVE/EDIT ACCT-INDX-CD EXISTENCE/EFFECTIVITY (ATI/ACI).
      ******************************************************************

           MOVE 'I1000-INQUIRY-ROUTINE' TO WSLG-PARAGRAPH.

           PERFORM S0500-SELECT-CURRENT-DATE.

           IF INQAINDX-START-DT = SPACES
               MOVE WS-CURR-DATE-ISO   TO INQAINDX-START-DT
               MOVE WS-CURR-DATE-ISO   TO WS-START-DT-ISO
           ELSE
               MOVE INQAINDX-START-DT  TO HV-ANY-DATE
               PERFORM S0800-SELECT-DATE-ISO-FORMAT

               IF  SQLCODE         = 0
               AND HV-ISO-DATE NOT > WS-CURR-DATE-ISO
                   MOVE HV-ISO-DATE    TO WS-START-DT-ISO
               ELSE
                   MOVE 'START-DT'     TO MAP-ELMNT-NAME-SY00
                   MOVE 000006         TO MSG-ID-SY00
      *            **** EFFECTIVE DATE INVALID
                   GO TO Z9999-ZZYII03-GETMESGS
               END-IF
           END-IF.

           MOVE INQAINDX-ACCT-INDX-CD  TO ATI-ACCT-INDX-CD.
           MOVE +0                     TO WS-SQLCOUNT.
           PERFORM S1000-COUNT-ATI.

           IF WS-SQLCOUNT = +0
               MOVE 'ACCT-INDX-CD'     TO MAP-ELMNT-NAME-SY00
               MOVE 000205             TO MSG-ID-SY00
      *        **** ACCOUNT INDEX INVALID
               GO TO Z9999-ZZYII03-GETMESGS
           END-IF.

           MOVE INQAINDX-ACCT-INDX-CD  TO ACI-FK-ATI-ACCT-INDXCD.
           MOVE WS-START-DT-ISO        TO ACI-START-DT.
           PERFORM S1100-SELECT-ACI-EFCTV.

           IF ACI-SQLCODE = +100
               MOVE 'ACCT-INDX-CD'     TO MAP-ELMNT-NAME-SY00
               MOVE 000198             TO MSG-ID-SY00
      *        **** REC NOT IN EFFECT FOR DATE REQUESTED
               GO TO Z9999-ZZYII03-GETMESGS
           END-IF.

           MOVE ACI-START-DT           TO INQAINDX-START-DT-OUT.
           MOVE ACI-END-DT             TO INQAINDX-END-DT.
           MOVE ACI-STATUS             TO INQAINDX-STATUS.
           MOVE ACI-ACCT-INDX-TTL      TO INQAINDX-ACCT-INDX-TTL.
           MOVE ACI-FUND-OVRDE         TO INQAINDX-FUND-OVRDE.
           MOVE ACI-ORGN-OVRDE         TO INQAINDX-ORGN-OVRDE.
           MOVE ACI-ACCT-OVRDE         TO INQAINDX-ACCT-OVRDE.
           MOVE ACI-PRGRM-OVRDE        TO INQAINDX-PRGRM-OVRDE.
           MOVE ACI-ACTVY-OVRDE        TO INQAINDX-ACTVY-OVRDE.
           MOVE ACI-LCTN-OVRDE         TO INQAINDX-LCTN-OVRDE.
           MOVE ACI-FUND-CD            TO INQAINDX-FUND-CD.
           MOVE ACI-ORGN-CD            TO INQAINDX-ORGN-CD.
           MOVE ACI-ACCT-CD            TO INQAINDX-ACCT-CD.
           MOVE ACI-PRGRM-CD           TO INQAINDX-PRGRM-CD.
           MOVE ACI-ACTVY-CD           TO INQAINDX-ACTVY-CD.
           MOVE ACI-LCTN-CD            TO INQAINDX-LCTN-CD.
           MOVE ACI-EARLY-INCTV-DT     TO INQAINDX-EARLY-INCTV-DT.
      /
       S0500-SELECT-CURRENT-DATE.
      ******************************************************************
      *    GET THE CURRENT DATE.
      ******************************************************************

           MOVE 'S0500-SELECT-CURRENT-DATE'
                                       TO WSLG-PARAGRAPH.
           MOVE   0500                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'SYSIBM.SYSDUMMY1'     TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT CHAR(CURRENT_DATE,ISO)
                 INTO :WS-CURR-DATE-ISO
                 FROM SYSIBM.SYSDUMMY1
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S0800-SELECT-DATE-ISO-FORMAT.
      ******************************************************************
      *    GET THE DATE IN ISO FORMAT.
      ******************************************************************

           MOVE 'S0800-SELECT-DATE-ISO-FORMAT'
                                       TO WSLG-PARAGRAPH.
           MOVE   0800                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'SYSIBM.SYSDUMMY1'     TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT CHAR(DATE(:HV-ANY-DATE),ISO)
                 INTO :HV-ISO-DATE
                 FROM SYSIBM.SYSDUMMY1
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
             WHEN -180
             WHEN -181
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S1000-COUNT-ATI.
      ******************************************************************
      *    SELECT/COUNT ATI_ACTI_V FOR EXISTENCE
      ******************************************************************

           MOVE 'S1000-COUNT-ATI'      TO WSLG-PARAGRAPH.
           MOVE   1000                 TO WSLG-SQL-NBR.
           MOVE 'COUNT'                TO WSLG-DB2-STATEMENT.
           MOVE 'ATI_ACTI_V'           TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT  COUNT(*)
                 INTO :WS-SQLCOUNT
                 FROM  ATI_ACTI_V
                WHERE  ACCT_INDX_CD = :ATI-ACCT-INDX-CD
           END-EXEC.

           MOVE SQLCODE                TO ATI-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S1100-SELECT-ACI-EFCTV.
      ******************************************************************
      *    SELECT ACI_ACTI_EFCTV_V BASED UPON THE START DATE
      ******************************************************************

           MOVE 'S1100-SELECT-ACI-EFCTV' TO WSLG-PARAGRAPH.
           MOVE   1100                   TO WSLG-SQL-NBR.
           MOVE 'SELECT'                 TO WSLG-DB2-STATEMENT.
           MOVE 'ACI_ACTI_EFCTV_V'       TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT
                  CHAR(START_DT,ISO)
                 ,CHAR(END_DT,ISO)
                 ,STATUS
                 ,ACCT_INDX_TTL
                 ,FUND_OVRDE
                 ,ORGN_OVRDE
                 ,ACCT_OVRDE
                 ,PRGRM_OVRDE
                 ,ACTVY_OVRDE
                 ,LCTN_OVRDE
                 ,FUND_CD
                 ,ORGN_CD
                 ,ACCT_CD
                 ,PRGRM_CD
                 ,ACTVY_CD
                 ,LCTN_CD
                 ,CHAR(EARLY_INCTV_DT,ISO)
               INTO
                  :ACI-START-DT
                 ,:ACI-END-DT
                 ,:ACI-STATUS
                 ,:ACI-ACCT-INDX-TTL
                 ,:ACI-FUND-OVRDE
                 ,:ACI-ORGN-OVRDE
                 ,:ACI-ACCT-OVRDE
                 ,:ACI-PRGRM-OVRDE
                 ,:ACI-ACTVY-OVRDE
                 ,:ACI-LCTN-OVRDE
                 ,:ACI-FUND-CD
                 ,:ACI-ORGN-CD
                 ,:ACI-ACCT-CD
                 ,:ACI-PRGRM-CD
                 ,:ACI-ACTVY-CD
                 ,:ACI-LCTN-CD
                 ,:ACI-EARLY-INCTV-DT
                FROM ACI_ACTI_EFCTV_V        A
               WHERE A.FK_ATI_ACCT_INDXCD = :ACI-FK-ATI-ACCT-INDXCD
                 AND A.END_DT            >= :ACI-START-DT
                 AND A.START_DT =
002MLJ              (SELECT MAX(B.START_DT)
                       FROM ACI_ACTI_EFCTV_V B
                      WHERE B.FK_ATI_ACCT_INDXCD
                                          = :ACI-FK-ATI-ACCT-INDXCD
                        AND B.END_DT     >= :ACI-START-DT
                        AND B.START_DT   <= :ACI-START-DT)
                 AND A.TIME_STMP =
002MLJ              (SELECT MAX(C.TIME_STMP)
                       FROM ACI_ACTI_EFCTV_V C
                      WHERE C.FK_ATI_ACCT_INDXCD
                                          = :ACI-FK-ATI-ACCT-INDXCD
                        AND C.END_DT     >= :ACI-START-DT
                        AND C.START_DT    = A.START_DT)
           END-EXEC.

           MOVE SQLCODE                TO ACI-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S1200-SELECT-YED.
      ******************************************************************
      *    CHECK IF USER HAS ADD PERMISSION FOR THE GA/INDEXCPY/MSTRINDX
      *    FIELD VALUE PASSED IN ACCT-INDX-CD.
      ******************************************************************

           MOVE 'S1200-SELECT-YED'       TO WSLG-PARAGRAPH.
           MOVE   1200                   TO WSLG-SQL-NBR.
           MOVE 'SELECT'                 TO WSLG-DB2-STATEMENT.
           MOVE 'YED_CNT_EXCD_V'         TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT COUNT(*)
                 INTO :WS-SQLCOUNT
                 FROM YED_CNT_EXCD_V YED
                WHERE YED.FK_YEH_SCR_USER_CD = :YED-FK-YEH-SCR-USER-CD
                  AND YED.FK_YEH_MODULE_CD   = 'GA'
                  AND YED.FK_YEH_SCRN_CD     = 'INDEXCPY'
                  AND YED.FK_YEH_FIELD_CD    = 'MSTRINDX'
                  AND YED.ADD_PRMSN_FLAG     = 'Y'
                  AND :HV-ACCT-INDX-CD BETWEEN YED.LOW_VALUE
                                           AND YED.HIGH_VALUE
           END-EXEC.

           MOVE SQLCODE                TO YED-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S1250-SELECT-YTS.
      ******************************************************************
      *    CHECK IF USER HAS ADD PERMISSION FOR THE GA/INDEXCPY SCREEN
      *    VIA A TEMPLATE CODE.
      ******************************************************************

           MOVE 'S1250-SELECT-YTS'       TO WSLG-PARAGRAPH.
           MOVE   1250                   TO WSLG-SQL-NBR.
           MOVE 'SELECT'                 TO WSLG-DB2-STATEMENT.
           MOVE 'YTS_UNT_PMS_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT COUNT(*)
                 INTO :WS-SQLCOUNT
                 FROM YUS_SEC_USR_V YUS
                     ,YTS_UNT_PMS_V YTS
                WHERE YUS.SCRTY_USER_CD     = :YUS-SCRTY-USER-CD
                  AND YTS.FK_YTH_SCRTY_UNIT =  YUS.FK1_YTH_SCRTY_UNIT
                  AND YTS.FK_YTH_TMPLT_CD   =  YUS.FK1_YTH_TMPLT_CD
                  AND YTS.MODULE_CD         = 'GA'
                  AND YTS.SCRN_CD           = 'INDEXCPY'
                  AND YTS.ADD_PRMSN_FLAG    = 'Y'
           END-EXEC.

           MOVE SQLCODE                TO YTS-SQLCODE.

           EVALUATE YTS-SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.

       S1255-SELECT-YTS-INDEXTBL.
      ******************************************************************
      *    CHECK IF USER HAS ADD PERMISSION FOR THE GA/INDEXTBL SCREEN
      *    VIA A TEMPLATE CODE.
      ******************************************************************

           MOVE 'S1255-SELECT-YTS-INDEXTBL' TO WSLG-PARAGRAPH.
           MOVE   1255                   TO WSLG-SQL-NBR.
           MOVE 'SELECT'                 TO WSLG-DB2-STATEMENT.
           MOVE 'YTS_UNT_PMS_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT COUNT(*)
                 INTO :WS-SQLCOUNT
                 FROM YUS_SEC_USR_V YUS
                     ,YTS_UNT_PMS_V YTS
                WHERE YUS.SCRTY_USER_CD     = :YUS-SCRTY-USER-CD
                  AND YTS.FK_YTH_SCRTY_UNIT =  YUS.FK1_YTH_SCRTY_UNIT
                  AND YTS.FK_YTH_TMPLT_CD   =  YUS.FK1_YTH_TMPLT_CD
                  AND YTS.MODULE_CD         = 'GA'
                  AND YTS.SCRN_CD           = 'INDEXTBL'
                  AND YTS.ADD_PRMSN_FLAG    = 'Y'
           END-EXEC.

           MOVE SQLCODE                TO YTS-SQLCODE.

           EVALUATE YTS-SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       Z9998-LOG-ERROR-VIA-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W TO LOG 2 ERROR MESSAGES TO THE WSLG
      *    TRANSIENT DATA QUEUE AND, IF REQUESTED, TO THE Z/OS CONSOLE.
      *    THE FIRST MESSAGE (SEQ# 0) CONTAINS DESCRIPTIVE ERROR TEXT.
      *    THE SECOND MESSAGE (SEQ# 1) CONTAINS DIAGNOSTIC INFORMATION.
      ******************************************************************

           MOVE '0'                    TO WSLG-MSG-SEQ-NUM.
           MOVE 'WGA022I'              TO WSLG-PROGRAM-NAME.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           MOVE '1'                    TO WSLG-MSG-SEQ-NUM.
           MOVE EIBTASKN               TO WSLG-CICS-TASK-NUMBER.
           MOVE WSLG-DIAGNOSTIC-AREA   TO WSLG-MSG-TEXT.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.


       Z9998-LINK-TO-PROGRAM-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W.  IF THE LINK FAILS,
      *    ABEND THE TASK WITH A 'WCIX' DUMP CODE.
      ******************************************************************

           EXEC CICS LINK
                PROGRAM (WSLG-PROGRAM-UIS000W)
                COMMAREA(WSLG-USING-LIST)
                RESP    (WSLG-CICS-RESP)
                RESP2   (WSLG-CICS-RESP2)
           END-EXEC.

           IF WSLG-CICS-RESP NOT = DFHRESP(NORMAL)
               EXEC CICS ABEND
                    ABCODE('WCIX')
               END-EXEC
           END-IF.
      /
       Z9999-ZZYII03-GETMESGS.
      ******************************************************************
      *    GET ERROR MESSAGES FROM MSL_MSG_LINE_V
      *    AND MOVE ERROR INFORMATION TO THE COMMAREA RETURN FIELDS.
      ******************************************************************

           MOVE 'Z9999-ZZYII03-GETMESGS'
                                       TO WSLG-PARAGRAPH.
           MOVE   9999                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'MSL_MSG_LINE_V'       TO WSLG-DB2-TABLE.

           MOVE MSG-ID-SY00            TO INQAINDX-RETURN-MSG-NUM.
           MOVE MAP-ELMNT-NAME-SY00    TO INQAINDX-RETURN-MSG-FIELD.

           MOVE 'FS'                   TO MSL-FK-MSG-MSG-KEY(1:2).
           MOVE MSG-ID-SY00            TO MSL-FK-MSG-MSG-KEY(3:6).

           EXEC SQL
               SELECT  MSG_1
                 INTO :MSL-MSG-1
                 FROM  MSL_MSG_LINE_V
                WHERE  FK_MSG_MSG_KEY = :MSL-FK-MSG-MSG-KEY
           END-EXEC.

           MOVE SQLCODE                TO MSL-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               MOVE MSL-MSG-1          TO INQAINDX-RETURN-MSG-TEXT
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.

           GO TO Z9999-RETURN-DATA-ERROR.
      /
       Z9999-RETURN-DATA-ERROR.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM WITH DATA ERROR(S).
      ******************************************************************

           SET INQAINDX-RESPONSE-ERROR TO TRUE.
           MOVE 'E'                    TO INQAINDX-RETURN-STATUS-CODE.
           MOVE  1                     TO INQAINDX-RETURN-MSG-COUNT.

           GO TO Z9999-RETURN-TO-CALLING-PGM.


       Z9999-RETURN-DB2-ABEND.
      ******************************************************************
      *    LOG THE FATAL DB2 ERROR VIA PROGRAM UIS000W AND ABEND THE
      *    TASK WITH A 'WSQL' DUMP CODE.
      ******************************************************************

           SET INQAINDX-RESPONSE-ERROR TO TRUE.
           MOVE 'F'                    TO INQAINDX-RETURN-STATUS-CODE.

           MOVE WSLG-FATAL-DB2-MSG-ID  TO WSLG-MSG-ID.
           MOVE WSLG-DB2-STATEMENT     TO WSLG-FATAL-DB2-STATEMENT.
           MOVE WSLG-DB2-TABLE         TO WSLG-FATAL-DB2-TABLE.
           MOVE WSLG-FATAL-DB2-MSG-TEXT
                                       TO WSLG-MSG-TEXT.
           MOVE 'Y'                    TO WSLG-CONSOLE-MSG-IND.
           MOVE SPACES                 TO WSLG-DIAGNOSTIC-INFO.
           MOVE SQLCODE                TO WSLG-SQL-CODE.
           STRING 'SQL#: '                WSLG-SQL-NBR
                  '  SQLCODE: '           WSLG-SQL-CODE
                   DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                     INTO WSLG-DIAGNOSTIC-INFO.
           PERFORM Z9998-LOG-ERROR-VIA-UIS000W.

           EXEC CICS ABEND
                ABCODE('WSQL')
           END-EXEC.


       Z9999-RETURN-SUCCESSFUL.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM WITH SUCCESSFUL STATUS.
      ******************************************************************

           SET INQAINDX-RESPONSE-SUCCESSFUL
                                       TO TRUE.
           MOVE 'S'                    TO INQAINDX-RETURN-STATUS-CODE.
           MOVE  1                     TO INQAINDX-RETURN-MSG-COUNT.
           MOVE SPACES                 TO INQAINDX-RETURN-MSG-NUM.
           MOVE SPACES                 TO INQAINDX-RETURN-MSG-FIELD.
           MOVE WSLG-SUCCESSFUL-MSG-TEXT
                                       TO INQAINDX-RETURN-MSG-TEXT.
           GO TO Z9999-RETURN-TO-CALLING-PGM.


       Z9999-RETURN-TO-CALLING-PGM.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM.
      ******************************************************************

           MOVE INQAINDX-PARM-AREA     TO DFHCOMMAREA (1:LENGTH OF
                INQAINDX-PARM-AREA).

           EXEC CICS RETURN
           END-EXEC.
