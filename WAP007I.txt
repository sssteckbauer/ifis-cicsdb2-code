       IDENTIFICATION DIVISION.
       PROGRAM-ID.    WAP007I.
      ******************************************************************
      *    TITLE ........... REPORT SCIQUEST INVOICES BY BATCH ID
      *                     (INQINV2)
      *    LANGUAGE ........ COBOL Z/OS, DB2, CICS
      *    AUTHOR .......... H. VANDERWEIT
      *    DATE-WRITTEN .... JULY, 2008
      *
      *                      DESCRIPTION
      *                      -----------
      *    THIS WEB SERVICE RECEIVES A SCIQUEST BATCH ID AND RETURNS
      *    SCIQUEST INVOICES WITH A MATCHING (FULL OR GENERIC) BATCH ID.
      *    IF THE SCIQUEST BATCH ID CONTAINS A DOT, REPLACE SUBSEQUENT
      *    CHARACTERS WITH '%' AND DO A LIKE/GENERIC SEARCH; OTHERWISE
      *    DO AN EQUAL/FULL SEARCH.
      *
      *    THE FOLLOWING FIELDS ARE RETURNED:
      *
      *      . RECORD-COUNT - THE NUMBER OF RECORDS RETURNED
      *          . DOC-NBR       - APH.DOC_NBR
      *          . APRVD-AMT     - IVH.INVD_AMT
      *
      *      . STARTING-DOC-NBR  - STARTING/CONTINUATION APH.DOC-NBR
      *
      *    DB2 TABLE(S)      HOW ACCESSED
      *    ----------------  -----------------------------------------
      *    APH_APRVL_HDR_V   FETCH
      *    IVH_INV_HDR_V     SELECT
      *
      ******************************************************************
      *                      MAINTENANCE HISTORY                       *
      ******************************************************************
      *    MODIFIED BY .....
      *    DATE MODIFIED ...
      *    MODIFICATION ....
      ******************************************************************
      /
       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
       01  FILLER                      PIC X(36)       VALUE
           'WAP007I WORKING-STORAGE BEGINS HERE'.

       01  WS-MISCELLANEOUS.
           05  WS-X                    PIC S9(4) COMP  VALUE ZEROES.
           05  WS-BATCH-ID                             VALUE SPACES.
               10  WS-BATCH-CHAR       PIC X(01)
                                       OCCURS 25 TIMES.
           05  WS-GENERIC-SEARCH-SW    PIC X(01)       VALUE 'N'.

       01  WSLG-USING-LIST.
           05  WSLG-CONSOLE-MSG-IND    PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-ID             PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-DATE           PIC X(10)       VALUE SPACES.
           05  WSLG-MSG-TIME           PIC X(08)       VALUE SPACES.
           05  WSLG-PROGRAM-NAME       PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-SEQ-NUM        PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-TEXT           PIC X(80)       VALUE SPACES.

       01  WSLG-DIAGNOSTIC-AREA.
           05  FILLER                  PIC X(05)       VALUE 'PARA:'.
           05  WSLG-PARAGRAPH          PIC X(30)       VALUE SPACES.
           05  FILLER                  PIC X(05)       VALUE 'TASK:'.
           05  WSLG-CICS-TASK-NUMBER   PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  WSLG-DIAGNOSTIC-INFO    PIC X(30)       VALUE SPACES.

       01  WSLG-MISC-AREA.
           05  WSLG-PROGRAM-UIS000W    PIC X(08)       VALUE 'UIS000W '.
           05  WSLG-CICS-RESP          PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-CICS-RESP2         PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-EIBCALEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-CHANLLEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-DB2-STATEMENT      PIC X(08)       VALUE SPACES.
           05  WSLG-DB2-TABLE          PIC X(16)       VALUE SPACES.
           05  WSLG-SQL-NBR            PIC 9(04)       VALUE ZEROES.
           05  WSLG-SQL-CODE           PIC +++++9      VALUE ZEROES.

       01  WSLG-MESSAGE-AREA.
           05  WSLG-SUCCESSFUL-MSG-ID  PIC X(08)       VALUE 'WFAP000I'.
           05  WSLG-SUCCESSFUL-MSG-TEXT
                                       PIC X(40)       VALUE
               'The request was successfully completed. '.

           05  WSLG-CHNL-ID-MSG-ID     PIC X(08)       VALUE 'WFAP001E'.
           05  WSLG-CHNL-ID-MSG-TEXT   PIC X(80)       VALUE
               'The received channel ID value is unrecognized.'.

           05  WSLG-CHNL-VERS-MSG-ID   PIC X(08)       VALUE 'WFAP002E'.
           05  WSLG-CHNL-VERS-MSG-TEXT PIC X(80)       VALUE
               'The received channel version value is unrecognized.'.

           05  WSLG-CHNL-LGTH-MSG-ID   PIC X(08)       VALUE 'WFAP003E'.
           05  WSLG-CHNL-LGTH-MSG-TEXT PIC X(80)       VALUE
               'The received channel length is not equal to the defined
      -        'channel version length. '.

           05  WSLG-FATAL-DB2-MSG-ID   PIC X(08)       VALUE 'WFAP004S'.
           05  WSLG-FATAL-DB2-MSG-TEXT.
               10  FILLER              PIC X(49)       VALUE
                   'DB2 returned a severe SQL error on an attempt to '.
               10  WSLG-FATAL-DB2-STATEMENT
                                       PIC X(08)       VALUE SPACES.
               10  FILLER              PIC X(07)       VALUE ' table '.
               10  WSLG-FATAL-DB2-TABLE
                                       PIC X(16)       VALUE SPACES.
      /
      ******************************************************************
      *01  INQINV2-PARM-AREA.
      ******************************************************************
           COPY WAP007I.
      /
      ******************************************************************
      *    SQL COMMAREA
      ******************************************************************
           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
      ******************************************************************
      *    APH_APRVL_HDR_V
      ******************************************************************
       01  APH-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'APH*'.
           05  APH-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVAPH
           END-EXEC.
      /
      ******************************************************************
      *    IVH_INV_HDR_V
      ******************************************************************
       01  IVH-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'IVH*'.
           05  IVH-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVIVH
           END-EXEC.
      /
       LINKAGE SECTION.
      /
       01  DFHCOMMAREA.
           05  COMM-CHANNEL-HEADER.
               10  COMM-CHANNEL-ID     PIC X(08).
                   88  COMM-CHANNEL-ID-OK              VALUE 'INQINV2 '.
               10  COMM-VERS-RESP-CD   PIC X(08).
                   88  COMM-VERSION-01-00-00           VALUE '01.00.00'.
           05  COMM-INQINV2-INPUT-PARMS
                                       PIC X(5000).

       PROCEDURE DIVISION.

      /
       A0000-MAINLINE-ROUTINE.
      ******************************************************************
      *    PERFORM ROUTINE TO EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      *    PERFORM ROUTINE TO RETRIEVE AND REPORT REQUESTED DOCUMENTS.
      *    RETURN CONTROL TO THE CLIENT APPLICATION.
      ******************************************************************

           MOVE 'A0000-MAINLINE-ROUTINE'   TO WSLG-PARAGRAPH.

           PERFORM A0500-EDIT-COMMAREA-VERSION.

           MOVE DFHCOMMAREA                TO INQINV2-PARM-AREA.
           INITIALIZE INQINV2-RETURN-STATUS-PARMS.
           INITIALIZE INQINV2-OUTPUT-PARMS.

           PERFORM A1000-INQUIRY-ROUTINE.

           GO TO Z9999-RETURN-SUCCESSFUL.
      /
       A0500-EDIT-COMMAREA-VERSION.
      ******************************************************************
      *    EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      ******************************************************************

           MOVE 'A0500-EDIT-COMMAREA-VERSION'
                                           TO WSLG-PARAGRAPH.
           INSPECT COMM-CHANNEL-ID
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           INITIALIZE                         INQINV2-PARM-AREA.
           MOVE COMM-CHANNEL-HEADER        TO INQINV2-CHANNEL-HEADER.

           IF COMM-CHANNEL-ID-OK
               EVALUATE TRUE
               WHEN COMM-VERSION-01-00-00

                   IF EIBCALEN = LENGTH OF INQINV2-PARM-AREA
                       NEXT SENTENCE
                   ELSE
                       SET INQINV2-RESPONSE-ERROR
                                           TO TRUE
                       MOVE 'E'            TO INQINV2-RETURN-STATUS-CODE
                       MOVE  1             TO INQINV2-RETURN-MSG-COUNT
                       MOVE 'A05001'       TO INQINV2-RETURN-MSG-NUM
                       MOVE 'Channel-Id'   TO INQINV2-RETURN-MSG-FIELD
                       MOVE 'The channel length is invalid           '
                                           TO INQINV2-RETURN-MSG-TEXT
                       MOVE WSLG-CHNL-LGTH-MSG-ID
                                           TO WSLG-MSG-ID
                       MOVE WSLG-CHNL-LGTH-MSG-TEXT
                                           TO WSLG-MSG-TEXT
                       MOVE 'N'            TO WSLG-CONSOLE-MSG-IND
                       MOVE SPACES         TO WSLG-DIAGNOSTIC-INFO
                       MOVE EIBCALEN       TO WSLG-EIBCALEN-NUMEDIT
                       MOVE LENGTH OF INQINV2-PARM-AREA
                                           TO WSLG-CHANLLEN-NUMEDIT
                       STRING 'EIBCALEN '     WSLG-EIBCALEN-NUMEDIT
                              ' versus '      WSLG-CHANLLEN-NUMEDIT
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                       PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                       GO TO Z9999-RETURN-TO-CALLING-PGM
                   END-IF

               WHEN OTHER
                   SET INQINV2-RESPONSE-ERROR
                                           TO TRUE
                   MOVE 'E'                TO INQINV2-RETURN-STATUS-CODE
                   MOVE  1                 TO INQINV2-RETURN-MSG-COUNT
                   MOVE 'A05002'           TO INQINV2-RETURN-MSG-NUM
                   MOVE 'Channel-Id'       TO INQINV2-RETURN-MSG-FIELD
                   MOVE 'The channel version is invalid          '
                                           TO INQINV2-RETURN-MSG-TEXT
                   MOVE WSLG-CHNL-VERS-MSG-ID
                                           TO WSLG-MSG-ID
                   MOVE WSLG-CHNL-VERS-MSG-TEXT
                                           TO WSLG-MSG-TEXT
                   MOVE 'N'                TO WSLG-CONSOLE-MSG-IND
                   MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
                   STRING 'Received version: ' COMM-VERS-RESP-CD
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                   PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                   GO TO Z9999-RETURN-TO-CALLING-PGM
               END-EVALUATE
           ELSE
               SET INQINV2-RESPONSE-ERROR  TO TRUE
               MOVE 'E'                    TO INQINV2-RETURN-STATUS-CODE
               MOVE  1                     TO INQINV2-RETURN-MSG-COUNT
               MOVE 'A05003'               TO INQINV2-RETURN-MSG-NUM
               MOVE 'Channel-Id'           TO INQINV2-RETURN-MSG-FIELD
               MOVE 'The channel id value is invalid         '
                                           TO INQINV2-RETURN-MSG-TEXT
               MOVE WSLG-CHNL-ID-MSG-ID    TO WSLG-MSG-ID
               MOVE WSLG-CHNL-ID-MSG-TEXT  TO WSLG-MSG-TEXT
               MOVE 'N'                    TO WSLG-CONSOLE-MSG-IND
               MOVE SPACES                 TO WSLG-DIAGNOSTIC-INFO
               STRING 'Received Channel Id: ' COMM-CHANNEL-ID
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
               PERFORM Z9998-LOG-ERROR-VIA-UIS000W
               GO TO Z9999-RETURN-TO-CALLING-PGM
           END-IF.
      /
       A1000-INQUIRY-ROUTINE.
      ******************************************************************
      *    IF THE SCIQUEST BATCH ID CONTAINS A DOT, REPLACE SUBSEQUENT
      *    CHARACTERS WITH '%' AND DO A LIKE/GENERIC SEARCH; OTHERWISE
      *    DO AN EQUAL/FULL SEARCH.
      *    PROCESS THE APH-CURSOR TO REPORT EVERY SCIQUEST INVOICE
      *    WITH A MATCHING (FULL OR GENERIC) BATCH ID.
      ******************************************************************

           MOVE 'A1000-INQUIRY-ROUTINE'
                                       TO WSLG-PARAGRAPH.

           IF INQINV2-SCIQ-BATCH-ID(1:1) > SPACE
               NEXT SENTENCE
           ELSE
               MOVE 'A10001'           TO INQINV2-RETURN-MSG-NUM
               MOVE 'SCIQ-BATCH-ID'    TO INQINV2-RETURN-MSG-FIELD
               MOVE 'SCIQ-BATCH-ID CANNOT BE BLANK           '
                                       TO INQINV2-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           MOVE INQINV2-SCIQ-BATCH-ID  TO WS-BATCH-ID.
           MOVE 'N'                    TO WS-GENERIC-SEARCH-SW.

           PERFORM WITH TEST BEFORE
                   VARYING WS-X FROM +1 BY +1
                     UNTIL WS-X   > +25
               IF WS-GENERIC-SEARCH-SW = 'Y'
                   MOVE '%'            TO WS-BATCH-CHAR (WS-X)
               ELSE
                   IF  WS-BATCH-CHAR (WS-X) = '.'
                   AND WS-X                 < +25
                       MOVE 'Y'        TO WS-GENERIC-SEARCH-SW
                   END-IF
               END-IF
           END-PERFORM.

           IF INQINV2-STARTING-DOC-NBR      = SPACES
           OR INQINV2-STARTING-DOC-NBR(1:1) = '8'
               NEXT SENTENCE
           ELSE
               MOVE 'A10002'           TO INQINV2-RETURN-MSG-NUM
               MOVE 'STARTING-DOC-NBR' TO INQINV2-RETURN-MSG-FIELD
               MOVE 'DOC-NBR MUST BE BLANK OR BEGIN WITH 8   '
                                       TO INQINV2-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           MOVE WS-BATCH-ID            TO APH-SCIQ-BATCH-ID.
           MOVE INQINV2-STARTING-DOC-NBR
                                       TO APH-DOC-NBR.
           MOVE  0003                  TO APH-FK-APL-SEQ-NBR.
           MOVE 'SCIQUEST'             TO APH-FK-APL-USER-ID.
           MOVE 'AAA'                  TO APH-FK-APL-APV-TPLT-CD.
           MOVE  0001                  TO APH-FK-APL-LVL-SEQ-NBR.

           PERFORM S1000-OPEN-APH-CURSOR.

           PERFORM S1100-FETCH-APH-CURSOR.

           PERFORM A2000-PROCESS-APH-CURSOR
               UNTIL APH-SQLCODE = +100.

           PERFORM S1200-CLOSE-APH-CURSOR.

           IF INQINV2-RECORD-COUNT = ZERO
               MOVE 'A10003'           TO INQINV2-RETURN-MSG-NUM
               MOVE 'SCIQ-BATCH-ID'    TO INQINV2-RETURN-MSG-FIELD
               MOVE 'NO RECORDS FOUND' TO INQINV2-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           MOVE SPACES                 TO INQINV2-STARTING-DOC-NBR.
      /
       A2000-PROCESS-APH-CURSOR.
      ******************************************************************
      *    REPORT EACH INVOICE WITH MATCHING (FULL OR GENERIC) BATCH ID.
      *    IF THE MAXIMUM COUNT IS REACHED, RETURN A WARNING MESSAGE
      *    WITH THE CONTINUATION DOC-NBR.
      *    RETRIEVE THE IVH.INVD_AMT FOR EACH INVOICE.
      *    RETRIEVE THE NEXT APH ROW.
      ******************************************************************

           MOVE 'A2000-PROCESS-APH-CURSOR'
                                       TO WSLG-PARAGRAPH.

           IF INQINV2-RECORD-COUNT = 200
               MOVE 'A20001'           TO INQINV2-RETURN-MSG-NUM
               MOVE 'SCIQ-BATCH-ID'    TO INQINV2-RETURN-MSG-FIELD
               MOVE 'MAX RECS RETURNED - CONTINUE AT DOC-NBR '
                                       TO INQINV2-RETURN-MSG-TEXT
               MOVE APH-DOC-NBR        TO INQINV2-STARTING-DOC-NBR
               GO TO Z9999-RETURN-DATA-WARNING
           END-IF.

           MOVE APH-DOC-NBR            TO IVH-DOC-NBR.
           PERFORM S2000-SELECT-IVH-AMT.

           ADD  +1                     TO INQINV2-RECORD-COUNT.
           MOVE INQINV2-RECORD-COUNT   TO WS-X.

           MOVE APH-DOC-NBR            TO INQINV2-DOC-NBR   (WS-X).
           MOVE IVH-INVD-AMT           TO INQINV2-APRVD-AMT (WS-X).

           PERFORM S1100-FETCH-APH-CURSOR.
      /
       S1000-OPEN-APH-CURSOR.
      ******************************************************************
      *    OPEN APH_APRVL_HDR_V CURSOR.
      *    APH-CURSOR1 IS USED FOR SCIQ_BATCH_ID LIKE/GENERIC SEARCH.
      *    APH-CURSOR2 IS USED FOR SCIQ_BATCH_ID EQUAL/FULL SEARCH.
      ******************************************************************

           MOVE 'S1000-OPEN-APH-CURSOR'
                                       TO WSLG-PARAGRAPH.
           MOVE 'OPEN'                 TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'      TO WSLG-DB2-TABLE.

           IF WS-GENERIC-SEARCH-SW = 'Y'
               MOVE 1001               TO WSLG-SQL-NBR
               EXEC SQL
                 DECLARE APH-CURSOR1 CURSOR FOR
                   SELECT DOC_NBR
                     FROM APH_APRVL_HDR_V
                    WHERE FK_APL_SEQ_NBR     = :APH-FK-APL-SEQ-NBR
                      AND FK_APL_USER_ID     = :APH-FK-APL-USER-ID
                      AND FK_APL_APV_TPLT_CD = :APH-FK-APL-APV-TPLT-CD
                      AND FK_APL_LVL_SEQ_NBR = :APH-FK-APL-LVL-SEQ-NBR
                      AND SCIQ_BATCH_ID   LIKE :APH-SCIQ-BATCH-ID
                      AND DOC_NBR           >= :APH-DOC-NBR
                      AND DOC_NBR         LIKE '8%'
                   ORDER BY
                          DOC_NBR
                   OPTIMIZE FOR 201 ROWS
                   FOR FETCH ONLY
               END-EXEC

               EXEC SQL
                   OPEN APH-CURSOR1
               END-EXEC
           ELSE
               MOVE 1002               TO WSLG-SQL-NBR
               EXEC SQL
                 DECLARE APH-CURSOR2 CURSOR FOR
                   SELECT DOC_NBR
                     FROM APH_APRVL_HDR_V
                    WHERE FK_APL_SEQ_NBR     = :APH-FK-APL-SEQ-NBR
                      AND FK_APL_USER_ID     = :APH-FK-APL-USER-ID
                      AND FK_APL_APV_TPLT_CD = :APH-FK-APL-APV-TPLT-CD
                      AND FK_APL_LVL_SEQ_NBR = :APH-FK-APL-LVL-SEQ-NBR
                      AND SCIQ_BATCH_ID      = :APH-SCIQ-BATCH-ID
                      AND DOC_NBR           >= :APH-DOC-NBR
                      AND DOC_NBR         LIKE '8%'
                   ORDER BY
                          DOC_NBR
                   OPTIMIZE FOR 201 ROWS
                   FOR FETCH ONLY
               END-EXEC

               EXEC SQL
                   OPEN APH-CURSOR2
               END-EXEC
           END-IF.

           MOVE SQLCODE                TO APH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S1100-FETCH-APH-CURSOR.
      ******************************************************************
      *    FETCH APH_APRVL_HDR_V CURSOR
      ******************************************************************

           MOVE 'S1100-FETCH-APH-CURSOR'
                                       TO WSLG-PARAGRAPH.
           MOVE 'FETCH'                TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'      TO WSLG-DB2-TABLE.

           IF WS-GENERIC-SEARCH-SW = 'Y'
               MOVE 1101               TO WSLG-SQL-NBR
               EXEC SQL
                   FETCH APH-CURSOR1
                    INTO :APH-DOC-NBR
               END-EXEC
           ELSE
               MOVE 1102               TO WSLG-SQL-NBR
               EXEC SQL
                   FETCH APH-CURSOR2
                    INTO :APH-DOC-NBR
               END-EXEC
           END-IF.

           MOVE SQLCODE                TO APH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S1200-CLOSE-APH-CURSOR.
      ******************************************************************
      *    CLOSE APH_APRVL_HDR_V CURSOR
      ******************************************************************

           MOVE 'S1200-CLOSE-APH-CURSOR'
                                       TO WSLG-PARAGRAPH.
           MOVE 'CLOSE'                TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'      TO WSLG-DB2-TABLE.

           IF WS-GENERIC-SEARCH-SW = 'Y'
               MOVE 1201               TO WSLG-SQL-NBR
               EXEC SQL
                   CLOSE APH-CURSOR1
               END-EXEC
           ELSE
               MOVE 1202               TO WSLG-SQL-NBR
               EXEC SQL
                   CLOSE APH-CURSOR2
               END-EXEC
           END-IF.

           MOVE SQLCODE                TO APH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2000-SELECT-IVH-AMT.
      ******************************************************************
      *    SELECT IVH.INVD_AMT.
      ******************************************************************

           MOVE 'S2000-SELECT-IVH-AMT' TO WSLG-PARAGRAPH.
           MOVE   2000                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'IVH_INV_HDR_V'        TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT  IVH.INVD_AMT
                 INTO :IVH-INVD-AMT
                 FROM  IVH_INV_HDR_V  IVH
                WHERE  IVH.DOC_NBR = :IVH-DOC-NBR
           END-EXEC.

           MOVE SQLCODE                TO IVH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       Z9998-LOG-ERROR-VIA-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W TO LOG 2 ERROR MESSAGES TO THE WSLG
      *    TRANSIENT DATA QUEUE AND, IF REQUESTED, TO THE Z/OS CONSOLE.
      *    THE FIRST MESSAGE (SEQ# 0) CONTAINS DESCRIPTIVE ERROR TEXT.
      *    THE SECOND MESSAGE (SEQ# 1) CONTAINS DIAGNOSTIC INFORMATION.
      ******************************************************************

           MOVE '0'                    TO WSLG-MSG-SEQ-NUM.
           MOVE 'WAP007I'              TO WSLG-PROGRAM-NAME.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           MOVE '1'                    TO WSLG-MSG-SEQ-NUM.
           MOVE EIBTASKN               TO WSLG-CICS-TASK-NUMBER.
           MOVE WSLG-DIAGNOSTIC-AREA   TO WSLG-MSG-TEXT.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.


       Z9998-LINK-TO-PROGRAM-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W.  IF THE LINK FAILS,
      *    ABEND THE TASK WITH A 'WCIX' DUMP CODE.
      ******************************************************************

           EXEC CICS LINK
                PROGRAM (WSLG-PROGRAM-UIS000W)
                COMMAREA(WSLG-USING-LIST)
                RESP    (WSLG-CICS-RESP)
                RESP2   (WSLG-CICS-RESP2)
           END-EXEC.

           IF WSLG-CICS-RESP NOT = DFHRESP(NORMAL)
               EXEC CICS ABEND
                    ABCODE('WCIX')
               END-EXEC
           END-IF.
      /
       Z9999-RETURN-DATA-ERROR.
      ******************************************************************
      *    RETURN THE DATA ERROR MESSAGE TO THE CALLING PROGRAM.
      ******************************************************************

           SET INQINV2-RESPONSE-ERROR  TO TRUE.
           MOVE 'E'                    TO INQINV2-RETURN-STATUS-CODE.
           MOVE  1                     TO INQINV2-RETURN-MSG-COUNT.

           GO TO Z9999-RETURN-TO-CALLING-PGM.


       Z9999-RETURN-DATA-WARNING.
      ******************************************************************
      *    RETURN THE DATA WARNING MESSAGE TO THE CALLING PROGRAM.
      ******************************************************************

           SET INQINV2-RESPONSE-WARNING
                                       TO TRUE.
           MOVE 'W'                    TO INQINV2-RETURN-STATUS-CODE.
           MOVE  1                     TO INQINV2-RETURN-MSG-COUNT.

           GO TO Z9999-RETURN-TO-CALLING-PGM.
      /
       Z9999-RETURN-DB2-ABEND.
      ******************************************************************
      *    LOG THE FATAL DB2 ERROR VIA PROGRAM UIS000W
      *    AND ABEND THE TASK WITH A 'WSQL' DUMP CODE.
      ******************************************************************

           MOVE WSLG-FATAL-DB2-MSG-ID  TO WSLG-MSG-ID.
           MOVE WSLG-DB2-STATEMENT     TO WSLG-FATAL-DB2-STATEMENT.
           MOVE WSLG-DB2-TABLE         TO WSLG-FATAL-DB2-TABLE.
           MOVE WSLG-FATAL-DB2-MSG-TEXT
                                       TO WSLG-MSG-TEXT.
           MOVE 'Y'                    TO WSLG-CONSOLE-MSG-IND.
           MOVE SPACES                 TO WSLG-DIAGNOSTIC-INFO.
           MOVE SQLCODE                TO WSLG-SQL-CODE.
           STRING 'SQL#: '                WSLG-SQL-NBR
                  '  SQLCODE: '           WSLG-SQL-CODE
                   DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                     INTO WSLG-DIAGNOSTIC-INFO.
           PERFORM Z9998-LOG-ERROR-VIA-UIS000W.

           EXEC CICS ABEND
                ABCODE('WSQL')
           END-EXEC.
      /
       Z9999-RETURN-SUCCESSFUL.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM WITH SUCCESSFUL STATUS.
      ******************************************************************

           SET INQINV2-RESPONSE-SUCCESSFUL
                                       TO TRUE.
           MOVE 'S'                    TO INQINV2-RETURN-STATUS-CODE.
           MOVE  1                     TO INQINV2-RETURN-MSG-COUNT.
           MOVE SPACES                 TO INQINV2-RETURN-MSG-NUM.
           MOVE SPACES                 TO INQINV2-RETURN-MSG-FIELD.
           MOVE WSLG-SUCCESSFUL-MSG-TEXT
                                       TO INQINV2-RETURN-MSG-TEXT.

           GO TO Z9999-RETURN-TO-CALLING-PGM.


       Z9999-RETURN-TO-CALLING-PGM.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM.
      ******************************************************************

           MOVE INQINV2-PARM-AREA      TO DFHCOMMAREA (1:LENGTH OF
                INQINV2-PARM-AREA).

           EXEC CICS RETURN
           END-EXEC.
