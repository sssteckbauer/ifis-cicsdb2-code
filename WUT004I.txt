       IDENTIFICATION DIVISION.
       PROGRAM-ID.    WUT004I.
      ******************************************************************
      *    TITLE ........... CHECK SECURITY AUTHORIZATION FOR USER ID
      *                      AND SCREEN (CHKAUTH1)
      *    LANGUAGE ........ COBOL Z/OS, DB2, CICS
      *    AUTHOR .......... M. JEWETT
      *    DATE-WRITTEN .... MARCH, 2008
      *
      *                      DESCRIPTION
      *                      -----------
      *    RETRIEVE INQUIRY, ADD, CHANGE AND DELETE AUTHORIZATION
      *    PERMISSIONS ('Y' OR 'N') FOR THE PASSED USER-ID,
      *    SUB-SYSTEM/MODULE CODE, AND SCREEN CODE.
      *
      *    DB2 TABLE(S)      HOW ACCESSED
      *    ----------------  -----------------------------------------
      *    YUN_USR_SCN       SELECT
      *    YTS_UNT_PMS       SELECT
      *    YUS_SEC_USR       SELECT
      *
      ******************************************************************
      *                      MAINTENANCE HISTORY                       *
      ******************************************************************
      *    MODIFIED BY .....
      *    DATE MODIFIED ...
      *    MODIFICATION ....
      ******************************************************************
      /
       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
       01  FILLER                      PIC X(36)       VALUE
           'WUT004I WORKING-STORAGE BEGINS HERE'.

       01  WS-MISC.
           05  WS-ADD-PERMISSION       PIC X(01)       VALUE SPACES.
           05  WS-CHG-PERMISSION       PIC X(01)       VALUE SPACES.
           05  WS-DEL-PERMISSION       PIC X(01)       VALUE SPACES.

       01  WSLG-USING-LIST.
           05  WSLG-CONSOLE-MSG-IND    PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-ID             PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-DATE           PIC X(10)       VALUE SPACES.
           05  WSLG-MSG-TIME           PIC X(08)       VALUE SPACES.
           05  WSLG-PROGRAM-NAME       PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-SEQ-NUM        PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-TEXT           PIC X(80)       VALUE SPACES.

       01  WSLG-DIAGNOSTIC-AREA.
           05  FILLER                  PIC X(05)       VALUE 'PARA:'.
           05  WSLG-PARAGRAPH          PIC X(30)       VALUE SPACES.
           05  FILLER                  PIC X(05)       VALUE 'TASK:'.
           05  WSLG-CICS-TASK-NUMBER   PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  WSLG-DIAGNOSTIC-INFO    PIC X(30)       VALUE SPACES.

       01  WSLG-MISC-AREA.
           05  WSLG-PROGRAM-UIS000W    PIC X(08)       VALUE 'UIS000W '.
           05  WSLG-CICS-RESP          PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-CICS-RESP2         PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-EIBCALEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-CHANLLEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-DB2-STATEMENT      PIC X(08)       VALUE SPACES.
           05  WSLG-DB2-TABLE          PIC X(16)       VALUE SPACES.
           05  WSLG-SQL-NBR            PIC 9(04)       VALUE ZEROES.
           05  WSLG-SQL-CODE           PIC +++++9      VALUE ZEROES.

       01  WSLG-MESSAGE-AREA.
           05  WSLG-SUCCESSFUL-MSG-ID  PIC X(08)       VALUE 'WFUT000I'.
           05  WSLG-SUCCESSFUL-MSG-TEXT
                                       PIC X(40)       VALUE
               'The request was successfully completed. '.

           05  WSLG-CHNL-ID-MSG-ID     PIC X(08)       VALUE 'WFUT001E'.
           05  WSLG-CHNL-ID-MSG-TEXT   PIC X(80)       VALUE
               'The received channel ID value is unrecognized.'.

           05  WSLG-CHNL-VERS-MSG-ID   PIC X(08)       VALUE 'WFUT002E'.
           05  WSLG-CHNL-VERS-MSG-TEXT PIC X(80)       VALUE
               'The received channel version value is unrecognized.'.

           05  WSLG-CHNL-LGTH-MSG-ID   PIC X(08)       VALUE 'WFUT003E'.
           05  WSLG-CHNL-LGTH-MSG-TEXT PIC X(80)       VALUE
               'The received channel length is not equal to the defined
      -        'channel version length. '.

           05  WSLG-FATAL-DB2-MSG-ID   PIC X(08)       VALUE 'WFUT004S'.
           05  WSLG-FATAL-DB2-MSG-TEXT.
               10  FILLER              PIC X(49)       VALUE
                   'DB2 returned a severe SQL error on an attempt to '.
               10  WSLG-FATAL-DB2-STATEMENT
                                       PIC X(08)       VALUE SPACES.
               10  FILLER              PIC X(07)       VALUE ' table '.
               10  WSLG-FATAL-DB2-TABLE
                                       PIC X(16)       VALUE SPACES.
      /
      ******************************************************************
      *01  CHKAUTH1-PARM-AREA.
      ******************************************************************
           COPY WUT004I.
      /
      ******************************************************************
      *    SQL COMMAREA
      ******************************************************************
           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
      ******************************************************************
      *    YUN_USR_SCN_V
      ******************************************************************
       01  YUN-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'YUN*'.
           05  YUN-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVYUN
           END-EXEC.
      /
      ******************************************************************
      *    YTS_UNT_PMS_V
      ******************************************************************
       01  YTS-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'YTS*'.
           05  YTS-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVYTS
           END-EXEC.
      /
      ******************************************************************
      *    YUS_SEC_USR_V
      ******************************************************************
       01  YUS-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'YUS*'.
           05  YUS-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVYUS
           END-EXEC.
      /
       LINKAGE SECTION.
      /
       01  DFHCOMMAREA.
           05  COMM-CHANNEL-HEADER.
               10  COMM-CHANNEL-ID     PIC X(08).
                   88  COMM-CHANNEL-ID-OK            VALUE 'CHKAUTH1'.
               10  COMM-VERS-RESP-CD   PIC X(08).
                   88  COMM-VERSION-01-00-00         VALUE '01.00.00'.
           05  COMM-CHKAUTH1-INPUT-PARMS
                                       PIC X(200).

       PROCEDURE DIVISION.

      /
       A0000-MAINLINE-ROUTINE.
      ******************************************************************
      *    PERFORM ROUTINE TO EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      *    PERFORM THE REQUESTED FUNCTION AND RETURN CONTROL WHEN
      *    COMPLETED SUCCESSFULLY.
      ******************************************************************

           MOVE 'A0000-MAINLINE-ROUTINE'   TO WSLG-PARAGRAPH.

           PERFORM A0500-EDIT-COMMAREA-VERSION.

           MOVE DFHCOMMAREA                TO CHKAUTH1-PARM-AREA.

           INSPECT CHKAUTH1-USER-ID
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
           INSPECT CHKAUTH1-SUBSYSTEM-CD
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
           INSPECT CHKAUTH1-SCREEN-CD
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           INITIALIZE CHKAUTH1-OUTPUT-PARMS.
           INITIALIZE CHKAUTH1-RETURN-STATUS-PARMS.

           PERFORM A1000-CHKAUTH-ROUTINE.

           GO TO Z9999-RETURN-SUCCESSFUL.
      /
       A0500-EDIT-COMMAREA-VERSION.
      ******************************************************************
      *    EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      ******************************************************************

           MOVE 'A0500-EDIT-COMMAREA-VERSION'
                                           TO WSLG-PARAGRAPH.
           INSPECT COMM-CHANNEL-ID
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           INITIALIZE                         CHKAUTH1-PARM-AREA.
           MOVE COMM-CHANNEL-HEADER        TO CHKAUTH1-CHANNEL-HEADER.

           IF COMM-CHANNEL-ID-OK
               EVALUATE TRUE
               WHEN COMM-VERSION-01-00-00

                   IF EIBCALEN = LENGTH OF CHKAUTH1-PARM-AREA
                       NEXT SENTENCE
                   ELSE
                       SET CHKAUTH1-RESPONSE-ERROR
                                           TO TRUE
                       MOVE 'E'         TO CHKAUTH1-RETURN-STATUS-CODE
                       MOVE  1             TO CHKAUTH1-RETURN-MSG-COUNT
                       MOVE 'A05001'       TO CHKAUTH1-RETURN-MSG-NUM
                       MOVE 'Channel-Id'   TO CHKAUTH1-RETURN-MSG-FIELD
                       MOVE 'The channel length is invalid           '
                                           TO CHKAUTH1-RETURN-MSG-TEXT
                       MOVE WSLG-CHNL-LGTH-MSG-ID
                                           TO WSLG-MSG-ID
                       MOVE WSLG-CHNL-LGTH-MSG-TEXT
                                           TO WSLG-MSG-TEXT
                       MOVE 'N'            TO WSLG-CONSOLE-MSG-IND
                       MOVE SPACES         TO WSLG-DIAGNOSTIC-INFO
                       MOVE EIBCALEN       TO WSLG-EIBCALEN-NUMEDIT
                       MOVE LENGTH OF CHKAUTH1-PARM-AREA
                                           TO WSLG-CHANLLEN-NUMEDIT
                       STRING 'EIBCALEN '     WSLG-EIBCALEN-NUMEDIT
                              ' versus '      WSLG-CHANLLEN-NUMEDIT
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                       PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                       GO TO Z9999-RETURN-TO-CALLING-PGM
                   END-IF

               WHEN OTHER
                   SET CHKAUTH1-RESPONSE-ERROR
                                           TO TRUE
                   MOVE 'E'             TO CHKAUTH1-RETURN-STATUS-CODE
                   MOVE  1                 TO CHKAUTH1-RETURN-MSG-COUNT
                   MOVE 'A05002'           TO CHKAUTH1-RETURN-MSG-NUM
                   MOVE 'Channel-Id'       TO CHKAUTH1-RETURN-MSG-FIELD
                   MOVE 'The channel version is invalid          '
                                           TO CHKAUTH1-RETURN-MSG-TEXT
                   MOVE WSLG-CHNL-VERS-MSG-ID
                                           TO WSLG-MSG-ID
                   MOVE WSLG-CHNL-VERS-MSG-TEXT
                                           TO WSLG-MSG-TEXT
                   MOVE 'N'                TO WSLG-CONSOLE-MSG-IND
                   MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
                   STRING 'Received version: ' COMM-VERS-RESP-CD
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                   PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                   GO TO Z9999-RETURN-TO-CALLING-PGM
               END-EVALUATE
           ELSE
               SET CHKAUTH1-RESPONSE-ERROR TO TRUE
               MOVE 'E'                 TO CHKAUTH1-RETURN-STATUS-CODE
               MOVE  1                     TO CHKAUTH1-RETURN-MSG-COUNT
               MOVE 'A05003'               TO CHKAUTH1-RETURN-MSG-NUM
               MOVE 'Channel-Id'           TO CHKAUTH1-RETURN-MSG-FIELD
               MOVE 'The channel id value is invalid         '
                                           TO CHKAUTH1-RETURN-MSG-TEXT
               MOVE WSLG-CHNL-ID-MSG-ID    TO WSLG-MSG-ID
               MOVE WSLG-CHNL-ID-MSG-TEXT  TO WSLG-MSG-TEXT
               MOVE 'N'                    TO WSLG-CONSOLE-MSG-IND
               MOVE SPACES                 TO WSLG-DIAGNOSTIC-INFO
               STRING 'Received Channel Id: ' COMM-CHANNEL-ID
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
               PERFORM Z9998-LOG-ERROR-VIA-UIS000W
               GO TO Z9999-RETURN-TO-CALLING-PGM
           END-IF.
      /
       A1000-CHKAUTH-ROUTINE.
      ******************************************************************
      *    CHECK SECURITY AUTHORIZATION FOR USER, SUBSYSTEM, AND SCREEN.
      *    IF SCREEN-SPECIFIC AUTHORIZATION DOESN'T EXIST ON THE YUN
      *    TABLE, CHECK FOR TEMPLATE AUTHORIZATION ON THE YTS/YUS TABLE.
      ******************************************************************

           MOVE 'A1000-CHKAUTH-ROUTINE'   TO WSLG-PARAGRAPH.

           MOVE CHKAUTH1-USER-ID       TO YUN-SCRTY-USER-CD.
           MOVE CHKAUTH1-SUBSYSTEM-CD  TO YUN-FK-YSC-MODULE-CD.
           MOVE CHKAUTH1-SCREEN-CD     TO YUN-FK-YSC-SCRN-CD.

           PERFORM S1000-SELECT-YUN.

           IF YUN-SQLCODE = +100
               PERFORM S2000-SELECT-YTS
           END-IF.

           IF YTS-SQLCODE = +100
               MOVE 'N'                TO CHKAUTH1-INQ-PERMISSION
               MOVE 'N'                TO CHKAUTH1-ADD-PERMISSION
               MOVE 'N'                TO CHKAUTH1-CHG-PERMISSION
               MOVE 'N'                TO CHKAUTH1-DEL-PERMISSION
           ELSE
               MOVE 'Y'                TO CHKAUTH1-INQ-PERMISSION
               MOVE WS-ADD-PERMISSION  TO CHKAUTH1-ADD-PERMISSION
               MOVE WS-CHG-PERMISSION  TO CHKAUTH1-CHG-PERMISSION
               MOVE WS-DEL-PERMISSION  TO CHKAUTH1-DEL-PERMISSION
           END-IF.
      /
       S1000-SELECT-YUN.
      ******************************************************************
      *    FOR THE SPECIFIED USER ID, MODULE, AND SCREEN, RETURN YUN
      *    ADD, CHANGE, AND DELETE PERMISSIONS ('Y' OR 'N').
      ******************************************************************

           MOVE 'S1000-SELECT-YUN'     TO WSLG-PARAGRAPH.
           MOVE   1000                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'YUN_USR_SCN_V'        TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT YUN.ADD_PRMSN_FLAG
                     ,YUN.CHNG_PRMSN_FLAG
                     ,YUN.DLT_PRMSN_FLAG
                INTO :WS-ADD-PERMISSION
                    ,:WS-CHG-PERMISSION
                    ,:WS-DEL-PERMISSION
                 FROM YUN_USR_SCN_V  YUN
                WHERE YUN.SCRTY_USER_CD      = :YUN-SCRTY-USER-CD
                  AND YUN.FK_YSC_MODULE_CD   = :YUN-FK-YSC-MODULE-CD
                  AND YUN.FK_YSC_SCRN_CD     = :YUN-FK-YSC-SCRN-CD
           END-EXEC.

           MOVE SQLCODE                TO YUN-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2000-SELECT-YTS.
      ******************************************************************
      *    FOR THE SPECIFIED USER ID, MODULE, AND SCREEN, RETURN YTS
      *    ADD, CHANGE, AND DELETE PERMISSIONS ('Y' OR 'N').
      ******************************************************************

           MOVE 'S2000-SELECT-YTS'     TO WSLG-PARAGRAPH.
           MOVE   2000                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'YTS_UNT_PMS_V'        TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT YTS.ADD_PRMSN_FLAG
                     ,YTS.CHNG_PRMSN_FLAG
                     ,YTS.DLT_PRMSN_FLAG
                INTO :WS-ADD-PERMISSION
                    ,:WS-CHG-PERMISSION
                    ,:WS-DEL-PERMISSION
                 FROM YTS_UNT_PMS_V  YTS
                     ,YUS_SEC_USR_V  YUS
                WHERE YUS.SCRTY_USER_CD      = :YUN-SCRTY-USER-CD
                  AND YTS.MODULE_CD          = :YUN-FK-YSC-MODULE-CD
                  AND YTS.SCRN_CD            = :YUN-FK-YSC-SCRN-CD
                  AND YUS.FK1_YTH_SCRTY_UNIT = YTS.FK_YTH_SCRTY_UNIT
                  AND YUS.FK1_YTH_TMPLT_CD   = YTS.FK_YTH_TMPLT_CD
           END-EXEC.

           MOVE SQLCODE                TO YTS-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       Z9998-LOG-ERROR-VIA-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W TO LOG 2 ERROR MESSAGES TO THE WSLG
      *    TRANSIENT DATA QUEUE AND, IF REQUESTED, TO THE Z/OS CONSOLE.
      *    THE FIRST MESSAGE (SEQ# 0) CONTAINS DESCRIPTIVE ERROR TEXT.
      *    THE SECOND MESSAGE (SEQ# 1) CONTAINS DIAGNOSTIC INFORMATION.
      ******************************************************************

           MOVE '0'                    TO WSLG-MSG-SEQ-NUM.
           MOVE 'WUT004I'              TO WSLG-PROGRAM-NAME.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           MOVE '1'                    TO WSLG-MSG-SEQ-NUM.
           MOVE EIBTASKN               TO WSLG-CICS-TASK-NUMBER.
           MOVE WSLG-DIAGNOSTIC-AREA   TO WSLG-MSG-TEXT.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.


       Z9998-LINK-TO-PROGRAM-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W.  IF THE LINK FAILS,
      *    ABEND THE TASK WITH A 'WCIX' DUMP CODE.
      ******************************************************************

           EXEC CICS LINK
                PROGRAM (WSLG-PROGRAM-UIS000W)
                COMMAREA(WSLG-USING-LIST)
                RESP    (WSLG-CICS-RESP)
                RESP2   (WSLG-CICS-RESP2)
           END-EXEC.

           IF WSLG-CICS-RESP NOT = DFHRESP(NORMAL)
               EXEC CICS ABEND
                    ABCODE('WCIX')
               END-EXEC
           END-IF.
      /
       Z9999-RETURN-DATA-ERROR.
      ******************************************************************
      *    RETURN THE DATA ERROR MESSAGE TO THE CALLING PROGRAM.
      ******************************************************************

           SET CHKAUTH1-RESPONSE-ERROR TO TRUE.
           MOVE 'E'                    TO CHKAUTH1-RETURN-STATUS-CODE.
           MOVE  1                     TO CHKAUTH1-RETURN-MSG-COUNT.

           GO TO Z9999-RETURN-TO-CALLING-PGM.
      /
       Z9999-RETURN-DB2-ABEND.
      ******************************************************************
      *    LOG THE FATAL DB2 ERROR VIA PROGRAM UIS000W
      *    AND ABEND THE TASK WITH A 'WSQL' DUMP CODE.
      ******************************************************************

           MOVE WSLG-FATAL-DB2-MSG-ID  TO WSLG-MSG-ID.
           MOVE WSLG-DB2-STATEMENT     TO WSLG-FATAL-DB2-STATEMENT.
           MOVE WSLG-DB2-TABLE         TO WSLG-FATAL-DB2-TABLE.
           MOVE WSLG-FATAL-DB2-MSG-TEXT
                                       TO WSLG-MSG-TEXT.
           MOVE 'Y'                    TO WSLG-CONSOLE-MSG-IND.
           MOVE SPACES                 TO WSLG-DIAGNOSTIC-INFO.
           MOVE SQLCODE                TO WSLG-SQL-CODE.
           STRING 'SQL#: '                WSLG-SQL-NBR
                  '  SQLCODE: '           WSLG-SQL-CODE
                   DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                     INTO WSLG-DIAGNOSTIC-INFO.
           PERFORM Z9998-LOG-ERROR-VIA-UIS000W.

           EXEC CICS ABEND
                ABCODE('WSQL')
           END-EXEC.
      /
       Z9999-RETURN-SUCCESSFUL.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM WITH SUCCESSFUL STATUS.
      ******************************************************************

           SET CHKAUTH1-RESPONSE-SUCCESSFUL
                                       TO TRUE.
           MOVE 'S'                    TO CHKAUTH1-RETURN-STATUS-CODE.
           MOVE  1                     TO CHKAUTH1-RETURN-MSG-COUNT.
           MOVE SPACES                 TO CHKAUTH1-RETURN-MSG-NUM.
           MOVE SPACES                 TO CHKAUTH1-RETURN-MSG-FIELD.
           MOVE WSLG-SUCCESSFUL-MSG-TEXT
                                       TO CHKAUTH1-RETURN-MSG-TEXT.

           GO TO Z9999-RETURN-TO-CALLING-PGM.


       Z9999-RETURN-TO-CALLING-PGM.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM.
      ******************************************************************

           MOVE CHKAUTH1-PARM-AREA     TO DFHCOMMAREA (1:LENGTH OF
                CHKAUTH1-PARM-AREA).

           EXEC CICS RETURN
           END-EXEC.
