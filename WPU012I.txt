       IDENTIFICATION DIVISION.
       PROGRAM-ID.    WPU012I.
      ******************************************************************
      *    PGM/CHANNEL ID .. WPU012I/INQPO3
      *    IVORY PROJECT ... GivenPO_CO-GetPO_RQTData
      *    WEB SERVICE ..... GivenPO_CO-GetPO_RQTData
      *    TITLE ........... Retrieve Purchase Order Data
      *    LANGUAGE ........ COBOL Z/OS, DB2, CICS
      *    AUTHOR .......... M.JEWETT
      *    DATE-WRITTEN .... FEBRUARY, 2010
      *
      *                      DESCRIPTION
      *                      -----------
      *    THIS WEB SERVICE RECEIVES A PO NUMBER AND CHANGE ORDER SEQ
      *    AND RETURNS THE FOLLOWING FIELDS FROM IFIS:
      *      . PO-CLS-CD      - FROM POH TABLE
      *      . VNDR_NAME      - FROM ENT OR PRS
      *      . BUYER-CD       - FROM POH TABLE
      *      . ORGN-CD        - FROM POA TABLE
      *      . ORGN-TITLE     - FROM ORE TABLE
      *      . ORDER_DT       - FROM POS TABLE
      *      . 1ST-CMDTY-CD   - 1ST ITEM COMMODITY CODE, FROM POD
      *      . 1ST-ACCT-CD    - 1ST ITEM ACCOUNT CODE, FROM POA
      *      . RESP-FAX-NBR   - RESPONSIBLE ENTITY FAX NBR, FROM POH
      *      . RESP-EMAIL-ADR - RESPONSIBLE ENTITY EMAIL ADR, FROM POH
      *      . RQT-EMAIL-ADR  - REQUESTOR EMAIL ADR, FROM RQT
      *
      *    THE INPUT FUNCTION FIELD IDENTIFIES THE FOLLOWING FUNCTIONS:
      *      . RETRIEVE = RETRIEVE ONLY SPECIFIED FIELDS
      *
      *    DB2 TABLE(S)      HOW ACCESSED
      *    ----------------  -----------------------------------------
      *    POH_PO_HDR_V       SELECT
      *    POS_PO_HDR_SEQ_V   SELECT
      *    POD_PO_DTL_V       SELECT
      *    POA_PO_ACCT_V      SELECT
      *    RQT_RQST_V         SELECT
      *    RQD_RQST_DTL_V     SELECT
      *    ORE_ORGN_EFCTV_V   SELECT
      *    ENT_ENTY_V         SELECT
      *    PRS_PRSN_V         SELECT
      *
      ******************************************************************
      *                      MAINTENANCE HISTORY
      ******************************************************************
      *    MODIFIED BY .....
      *    DATE MODIFIED ...
      *    MODIFICATION ....
      ******************************************************************
      /
       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
       01  FILLER                      PIC X(36)       VALUE
               'WPU012I WORKING-STORAGE BEGINS HERE '.

       01  WS-MISCELLANEOUS.
           05  WS-NULL-INDICATOR       PIC S9(4)       COMP
                                                       VALUE ZEROES.
           05  WS-UPDATE-COUNT         PIC S9(4)       COMP
                                                       VALUE ZEROES.
           05  WS-INV-COUNT            PIC S9(5)       COMP-3
                                                       VALUE ZEROES.
           05  WS-INV-AMT              PIC S9(9)V99    COMP-3
                                                       VALUE ZEROES.

       01  WSLG-USING-LIST.
           05  WSLG-CONSOLE-MSG-IND    PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-ID             PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-DATE           PIC X(10)       VALUE SPACES.
           05  WSLG-MSG-TIME           PIC X(08)       VALUE SPACES.
           05  WSLG-PROGRAM-NAME       PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-SEQ-NUM        PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-TEXT           PIC X(80)       VALUE SPACES.

       01  WSLG-DIAGNOSTIC-AREA.
           05  FILLER                  PIC X(05)       VALUE 'PARA:'.
           05  WSLG-PARAGRAPH          PIC X(30)       VALUE SPACES.
           05  FILLER                  PIC X(05)       VALUE 'TASK:'.
           05  WSLG-CICS-TASK-NUMBER   PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  WSLG-DIAGNOSTIC-INFO    PIC X(30)       VALUE SPACES.

       01  WSLG-MISC-AREA.
           05  WSLG-PROGRAM-UIS000W    PIC X(08)       VALUE 'UIS000W '.
           05  WSLG-CICS-RESP          PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-CICS-RESP2         PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-EIBCALEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-CHANLLEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-DB2-STATEMENT      PIC X(08)       VALUE SPACES.
           05  WSLG-DB2-TABLE          PIC X(16)       VALUE SPACES.
           05  WSLG-SQL-NBR            PIC 9(04)       VALUE ZEROES.
           05  WSLG-SQL-CODE           PIC +++++9      VALUE ZEROES.

       01  WSLG-MESSAGE-AREA.
           05  WSLG-SUCCESSFUL-MSG-ID  PIC X(08)       VALUE 'WFPU000I'.
           05  WSLG-SUCCESSFUL-MSG-TEXT
                                       PIC X(40)       VALUE
               'The request was successfully completed. '.

           05  WSLG-CHNL-ID-MSG-ID     PIC X(08)       VALUE 'WFPU001E'.
           05  WSLG-CHNL-ID-MSG-TEXT   PIC X(80)       VALUE
               'The received channel ID value is unrecognized.'.

           05  WSLG-CHNL-VERS-MSG-ID   PIC X(08)       VALUE 'WFPU002E'.
           05  WSLG-CHNL-VERS-MSG-TEXT PIC X(80)       VALUE
               'The received channel version value is unrecognized.'.

           05  WSLG-CHNL-LGTH-MSG-ID   PIC X(08)       VALUE 'WFPU003E'.
           05  WSLG-CHNL-LGTH-MSG-TEXT PIC X(80)       VALUE
               'The received channel length is not equal to the defined
      -        'channel version length. '.

           05  WSLG-FATAL-DB2-MSG-ID   PIC X(08)       VALUE 'WFPU004S'.
           05  WSLG-FATAL-DB2-MSG-TEXT.
               10  FILLER              PIC X(49)       VALUE
                   'DB2 returned a severe SQL error on an attempt to '.
               10  WSLG-FATAL-DB2-STATEMENT
                                       PIC X(08)       VALUE SPACES.
               10  FILLER              PIC X(07)       VALUE ' table '.
               10  WSLG-FATAL-DB2-TABLE
                                       PIC X(16)       VALUE SPACES.
      /
      ******************************************************************
      *01  INQPO3-PARM-AREA.
      ******************************************************************
           COPY WPU012I.
      /
      ******************************************************************
      *    SQL COMMAREA
      ******************************************************************
           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      ******************************************************************
      *     ENT_ENTY_V
      ******************************************************************
       01  ENT-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'ENT*'.
           05  ENT-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVENT
           END-EXEC.
      /
      ******************************************************************
      *     PRS_PRSN_V
      ******************************************************************
       01  PRS-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'PRS*'.
           05  PRS-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPRS
           END-EXEC.
      /
      /
      ******************************************************************
      *     POH_PO_HDR_V
      ******************************************************************
       01  POH-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'POH*'.
           05  POH-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPOH
           END-EXEC.
      /
      ******************************************************************
      *    POS_PO_HDR_SEQ_V
      ******************************************************************
       01  POS-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'POS*'.
           05  POS-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPOS
           END-EXEC.
      /
      ******************************************************************
      *    POD_PO_DTL_V
      ******************************************************************
       01  POD-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'POD*'.
           05  POD-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPOD
           END-EXEC.
      /
      ******************************************************************
      *    POA_PO_ACCT_V
      ******************************************************************
       01  POA-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'POA*'.
           05  POA-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVPOA
           END-EXEC.
      /
      ******************************************************************
      *    RQT_RQST_V
      ******************************************************************
       01  RQT-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'RQT*'.
           05  RQT-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVRQT
           END-EXEC.
      /
      ******************************************************************
      *    RQD_RQST_DTL_V
      ******************************************************************
       01  RQD-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'RQD*'.
           05  RQD-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVRQD
           END-EXEC.
      /
      ******************************************************************
      *    ORE_ORGN_EFCTV_V
      ******************************************************************
       01  ORE-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'ORE*'.
           05  ORE-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVORE
           END-EXEC.
      /
       LINKAGE SECTION.
      /
       01  DFHCOMMAREA.
           05  COMM-CHANNEL-HEADER.
               10  COMM-CHANNEL-ID     PIC X(08).
                   88  COMM-CHANNEL-ID-OK              VALUE 'INQPO3'.
               10  COMM-VERS-RESP-CD   PIC X(08).
                   88  COMM-VERSION-01-00-00           VALUE '01.00.00'.
           05  COMM-INQPO3-INPUT-PARMS
                                       PIC X(500).

       PROCEDURE DIVISION.

      /
       A0000-MAINLINE-ROUTINE.
      ******************************************************************
      *    PERFORM ROUTINE TO EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      *    PERFORM THE REQUESTED FUNCTION AND RETURN CONTROL WHEN
      *    COMPLETED SUCCESSFULLY.
      ******************************************************************

           MOVE 'A0000-MAINLINE-ROUTINE'
                                       TO WSLG-PARAGRAPH.

           PERFORM A0500-EDIT-COMMAREA-VERSION.

           MOVE DFHCOMMAREA            TO INQPO3-PARM-AREA.
           INITIALIZE                     INQPO3-OUTPUT-PARMS.
           INITIALIZE                     INQPO3-RETURN-STATUS-PARMS.

           EVALUATE INQPO3-FUNCTION

             WHEN 'RETRIEVE'
               PERFORM A1000-RETRIEVE-ROUTINE

             WHEN OTHER
               MOVE 'A00001'           TO INQPO3-RETURN-MSG-NUM
               MOVE 'Function'         TO INQPO3-RETURN-MSG-FIELD
               MOVE 'The requested function is invalid       '
                                       TO INQPO3-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-EVALUATE.

           GO TO Z9999-RETURN-SUCCESSFUL.
      /
       A0500-EDIT-COMMAREA-VERSION.
      ******************************************************************
      *    EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      ******************************************************************

           MOVE 'A0500-EDIT-COMMAREA-VERSION'
                                       TO WSLG-PARAGRAPH.
           INSPECT COMM-CHANNEL-ID
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           INITIALIZE                     INQPO3-PARM-AREA.
           MOVE COMM-CHANNEL-HEADER    TO INQPO3-CHANNEL-HEADER.

           IF COMM-CHANNEL-ID-OK
               EVALUATE TRUE
               WHEN COMM-VERSION-01-00-00

                   IF EIBCALEN = LENGTH OF INQPO3-PARM-AREA
                       NEXT SENTENCE
                   ELSE
                       SET INQPO3-RESPONSE-ERROR
                                       TO TRUE
                       MOVE 'E'        TO INQPO3-RETURN-STATUS-CODE     E
                       MOVE  1         TO INQPO3-RETURN-MSG-COUNT
                       MOVE 'A05001'   TO INQPO3-RETURN-MSG-NUM
                       MOVE 'Channel-Id'
                                       TO INQPO3-RETURN-MSG-FIELD
                       MOVE 'The channel length is invalid           '
                                       TO INQPO3-RETURN-MSG-TEXT
                       MOVE WSLG-CHNL-LGTH-MSG-ID
                                       TO WSLG-MSG-ID
                       MOVE WSLG-CHNL-LGTH-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                       MOVE 'N'        TO WSLG-CONSOLE-MSG-IND
                       MOVE SPACES     TO WSLG-DIAGNOSTIC-INFO
                       MOVE EIBCALEN   TO WSLG-EIBCALEN-NUMEDIT
                       MOVE LENGTH OF INQPO3-PARM-AREA
                                       TO WSLG-CHANLLEN-NUMEDIT
                       STRING 'EIBCALEN ' WSLG-EIBCALEN-NUMEDIT
                              ' versus '  WSLG-CHANLLEN-NUMEDIT
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                       PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                       GO TO Z9999-RETURN-TO-CALLING-PGM
                   END-IF

               WHEN OTHER
                   SET INQPO3-RESPONSE-ERROR
                                       TO TRUE
                   MOVE 'E'            TO INQPO3-RETURN-STATUS-CODE
                   MOVE  1             TO INQPO3-RETURN-MSG-COUNT
                   MOVE 'A05002'       TO INQPO3-RETURN-MSG-NUM
                   MOVE 'Channel-Id'   TO INQPO3-RETURN-MSG-FIELD
                   MOVE 'The channel version is invalid          '
                                       TO INQPO3-RETURN-MSG-TEXT
                   MOVE WSLG-CHNL-VERS-MSG-ID
                                       TO WSLG-MSG-ID
                   MOVE WSLG-CHNL-VERS-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                   MOVE 'N'            TO WSLG-CONSOLE-MSG-IND
                   MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
                   STRING 'Received version: ' COMM-VERS-RESP-CD
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                   PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                   GO TO Z9999-RETURN-TO-CALLING-PGM
               END-EVALUATE
           ELSE
               SET INQPO3-RESPONSE-ERROR
                                       TO TRUE
               MOVE 'E'                TO INQPO3-RETURN-STATUS-CODE
               MOVE  1                 TO INQPO3-RETURN-MSG-COUNT
               MOVE 'A05003'           TO INQPO3-RETURN-MSG-NUM
               MOVE 'Channel-Id'       TO INQPO3-RETURN-MSG-FIELD
               MOVE 'The channel id value is invalid         '
                                       TO INQPO3-RETURN-MSG-TEXT
               MOVE WSLG-CHNL-ID-MSG-ID
                                       TO WSLG-MSG-ID
               MOVE WSLG-CHNL-ID-MSG-TEXT
                                       TO WSLG-MSG-TEXT
               MOVE 'N'                TO WSLG-CONSOLE-MSG-IND
               MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
               STRING 'Received Channel Id: ' COMM-CHANNEL-ID
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
               PERFORM Z9998-LOG-ERROR-VIA-UIS000W
               GO TO Z9999-RETURN-TO-CALLING-PGM
           END-IF.
      /
       A1000-RETRIEVE-ROUTINE.
      ******************************************************************
      *    RETRIEVE THE PO AND RQT DATA BASED ON THE PASSED PO NBR AND
      *    CHANGE ORDER SEQ NBR.
      ******************************************************************

           MOVE 'A1000-RETRIEVE-ROUTINE'
                                       TO WSLG-PARAGRAPH.

      *    IF INQPO3-USER-CD = SPACES
      *        MOVE 'A10001'           TO INQPO3-RETURN-MSG-NUM
      *        MOVE 'User-Cd'          TO INQPO3-RETURN-MSG-FIELD
      *        MOVE 'Required field is missing'
      *                                TO INQPO3-RETURN-MSG-TEXT
      *        GO TO Z9999-RETURN-DATA-ERROR
      *    END-IF.

      *    IF INQPO3-BATCH-ID = SPACES
      *        MOVE 'A10002'           TO INQPO3-RETURN-MSG-NUM
      *        MOVE 'Batch-Id'         TO INQPO3-RETURN-MSG-FIELD
      *        MOVE 'Required field is missing'
      *                                TO INQPO3-RETURN-MSG-TEXT
      *        GO TO Z9999-RETURN-DATA-ERROR
      *    END-IF.

           MOVE INQPO3-PO-NBR          TO POH-PO-NBR
                                          POS-FK-POH-PO-NBR
                                          POD-FK-POS-PO-NBR
                                          POA-FK-POD-PO-NBR
                                          RQD-FK-POD-PO-NBR.

           MOVE INQPO3-CHNG-SEQ-NBR    TO POS-CHNG-SEQ-NBR
                                          POD-FK-POS-CHG-SEQ-NBR
                                          POA-FK-POD-CHG-SEQ-NBR
                                          RQD-FK-POD-CHG-SEQ-NBR.

           PERFORM S1000-SELECT-POH.
           PERFORM S2000-SELECT-POS.
           PERFORM S3000-SELECT-POD.
           PERFORM S4000-SELECT-POA.
           PERFORM S5000-SELECT-ORE.
           PERFORM S6000-GET-RQT.
           PERFORM S7000-GET-VNDR-NAME.


      /
       S1000-SELECT-POH.
      ******************************************************************
      *    SELECT POH_PO_HDR_V.
      ******************************************************************

           MOVE 'S1000-SELECT-POH'     TO WSLG-PARAGRAPH.
           MOVE   1000                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'POH_PO_HDR_V'         TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT  POH.PO_CLS_CD
                      ,POH.BUYER_CD
                      ,POH.FK_VDR_IREF_ID
                      ,POH.FK_VDR_ENTPSN_IND
                      ,POH.RESP_FAX_NBR
                      ,POH.RESP_EMAIL_ADR
                 INTO :POH-PO-CLS-CD
                     ,:POH-BUYER-CD
                     ,:POH-FK-VDR-IREF-ID
                     ,:POH-FK-VDR-ENTPSN-IND
                     ,:POH-RESP-FAX-NBR
                     ,:POH-RESP-EMAIL-ADR
                 FROM  POH_PO_HDR_V   POH
                WHERE  POH.PO_NBR  = :POH-PO-NBR
           END-EXEC.

           MOVE SQLCODE                TO POH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.

           MOVE POH-PO-CLS-CD      TO INQPO3-PO-CLS-CD.
           MOVE POH-BUYER-CD       TO INQPO3-BUYER-CD.
           MOVE POH-RESP-FAX-NBR   TO INQPO3-RESP-FAX-NBR.
           MOVE POH-RESP-EMAIL-ADR TO INQPO3-RESP-EMAIL-ADR.
      /
       S2000-SELECT-POS.
      ******************************************************************
      *    SELECT POS_PO_HDR_SEQ_V.
      ******************************************************************

           MOVE 'S2000-SELECT-POS'     TO WSLG-PARAGRAPH.
           MOVE   2000                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'POS_PO_HDR_SEQ_V'     TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT  POS.ORDER_DT
                 INTO :POS-ORDER-DT
                 FROM  POS_PO_HDR_SEQ_V  POS
                WHERE POS.FK_POH_PO_NBR = :POS-FK-POH-PO-NBR
                  AND POS.CHNG_SEQ_NBR  = :POS-CHNG-SEQ-NBR
           END-EXEC.

           MOVE SQLCODE                TO POS-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.

           MOVE POS-ORDER-DT       TO INQPO3-ORDER-DT.
      /
       S3000-SELECT-POD.
      ******************************************************************
      *    SELECT POD_PO_DTL_V.
      ******************************************************************

           MOVE 'S3000-SELECT-POD'     TO WSLG-PARAGRAPH.
           MOVE   3000                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'POD_PO_DTL_V'         TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT  POD.FK_CDY_CMDTY_CD
                 INTO :POD-FK-CDY-CMDTY-CD
                 FROM  POD_PO_DTL_V  POD
                WHERE POD.FK_POS_PO_NBR      = :POD-FK-POS-PO-NBR
                  AND POD.FK_POS_CHG_SEQ_NBR = :POD-FK-POS-CHG-SEQ-NBR
                  AND POD.ITEM_NBR =
                     (SELECT MIN(B.ITEM_NBR)
                        FROM POD_PO_DTL_V  B
                  WHERE B.FK_POS_PO_NBR      = :POD-FK-POS-PO-NBR
                    AND B.FK_POS_CHG_SEQ_NBR = :POD-FK-POS-CHG-SEQ-NBR)
           END-EXEC.

           MOVE SQLCODE                TO POD-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.

           MOVE POD-FK-CDY-CMDTY-CD TO INQPO3-1ST-CMDTY-CD.
      /
       S4000-SELECT-POA.
      ******************************************************************
      *    SELECT POA_PO_ACCT_V.
      ******************************************************************

           MOVE 'S4000-SELECT-POA'     TO WSLG-PARAGRAPH.
           MOVE   4000                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'POA_PO_ACCT_V'        TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT  POA.ORGN_CD
                      ,POA.ACCT_CD
                 INTO :POA-ORGN-CD
                     ,:POA-ACCT-CD
                 FROM  POA_PO_ACCT_V POA
                WHERE POA.FK_POD_PO_NBR      = :POA-FK-POD-PO-NBR
                  AND POA.FK_POD_CHG_SEQ_NBR = :POA-FK-POD-CHG-SEQ-NBR
                  AND POA.FK_POD_ITEM_NBR    =
                     (SELECT MIN(B.FK_POD_ITEM_NBR)
                        FROM POA_PO_ACCT_V B
                  WHERE B.FK_POD_PO_NBR      = POA.FK_POD_PO_NBR
                    AND B.FK_POD_CHG_SEQ_NBR = POA.FK_POD_CHG_SEQ_NBR)
                  AND POA.SEQ_NBR    =
                     (SELECT MIN(C.SEQ_NBR)
                        FROM POA_PO_ACCT_V C
                  WHERE C.FK_POD_PO_NBR      = POA.FK_POD_PO_NBR
                    AND C.FK_POD_CHG_SEQ_NBR = POA.FK_POD_CHG_SEQ_NBR
                    AND C.FK_POD_ITEM_NBR    = POA.FK_POD_ITEM_NBR)
           END-EXEC.

           MOVE SQLCODE                TO POD-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.

           MOVE POA-ORGN-CD TO INQPO3-ORGN-CD.
           MOVE POA-ACCT-CD TO INQPO3-1ST-ACCT-CD.
      /
       S5000-SELECT-ORE.
      ******************************************************************
      *    SELECT ORE_ORGN_EFCTV_V.
      ******************************************************************

           MOVE 'S5000-SELECT-ORE'     TO WSLG-PARAGRAPH.
           MOVE   5000                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'ORE_ORGN_EFCTV_V'     TO WSLG-DB2-TABLE.
           MOVE POS-ORDER-DT           TO ORE-START-DT.

           EXEC SQL
               SELECT  ORE.ORGN_TITLE
                 INTO :ORE-ORGN-TITLE
                 FROM  ORE_ORGN_EFCTV_V ORE
                WHERE ORE.ORGN_CD    = :POA-ORGN-CD
                   AND ORE.START_DT =
                      (SELECT MAX(B.START_DT)
                         FROM ORE_ORGN_EFCTV_V B
                        WHERE B.ORGN_CD = :POA-ORGN-CD
                          AND B.END_DT     >= :ORE-START-DT
                          AND B.START_DT   <= :ORE-START-DT)
                   AND ORE.TIME_STMP =
                      (SELECT MAX(C.TIME_STMP)
                         FROM ORE_ORGN_EFCTV_V C
                        WHERE C.ORGN_CD = ORE.ORGN_CD
                          AND C.END_DT      = ORE.END_DT
                          AND C.START_DT    = ORE.START_DT)
           END-EXEC.

           MOVE SQLCODE                TO POD-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN +100
               MOVE 'NO ORGN TITLE FOUND' TO ORE-ORGN-TITLE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.

           MOVE ORE-ORGN-TITLE TO INQPO3-ORGN-TITLE.
      /
       S6000-GET-RQT.
      ******************************************************************
      *    RETIREVE DATA FROM RQT_RQST_V BUT MUST GO TO RQD_RQST_DTL_V
      *    FIRST USING PASSED PO-NBR AND CHNG-SEQ-NBR TO GET RQST-CD
      ******************************************************************

           MOVE 'S6000-GET-RQT'        TO WSLG-PARAGRAPH.
           MOVE   6000                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'RQD_RQST_DTL_V'       TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT  RQD.FK_RQT_RQST_CD
                 INTO :RQD-FK-RQT-RQST-CD
                 FROM  RQD_RQST_DTL_V  RQD
                WHERE RQD.FK_POD_PO_NBR      = :RQD-FK-POD-PO-NBR
                  AND RQD.FK_POD_CHG_SEQ_NBR = :RQD-FK-POD-CHG-SEQ-NBR
                  AND RQD.FK_POD_ITEM_NBR    = 0001
           END-EXEC.

           MOVE SQLCODE                TO RQD-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.

           MOVE RQD-FK-RQT-RQST-CD TO RQT-RQST-CD.

           MOVE 'S6000-GET-RQT'        TO WSLG-PARAGRAPH.
           MOVE   6050                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'RQT_RQST_V'           TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT  RQT.EMAIL_ADR
                 INTO :RQT-EMAIL-ADR
                 FROM  RQT_RQST_V  RQT
                WHERE RQT.RQST_CD   = :RQT-RQST-CD
           END-EXEC.

           MOVE SQLCODE                TO RQT-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN +100
               MOVE 'NO EMAIL ADR FOUND' TO RQT-EMAIL-ADR
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.

           MOVE RQT-EMAIL-ADR TO INQPO3-RQT-EMAIL-ADR.
      /
       S7000-GET-VNDR-NAME.
      ******************************************************************
      *    RETIREVE DATA FROM PRS_PRSN_V OR ENT_ENTY_V TO
      *    GET VENDOR NAME
      ******************************************************************
           IF POH-FK-VDR-ENTPSN-IND = 'E'

              MOVE 'S7000-GET-VNDR-NAME'  TO WSLG-PARAGRAPH
              MOVE   7000                 TO WSLG-SQL-NBR
              MOVE 'SELECT'               TO WSLG-DB2-STATEMENT
              MOVE 'ENT_ENTY_V'           TO WSLG-DB2-TABLE

              EXEC SQL
                 SELECT ENT.NAME_KEY
                   INTO :ENT-NAME-KEY
                   FROM ENT_ENTY_V  ENT
                  WHERE ENT.IREF_ID = :POH-FK-VDR-IREF-ID
              END-EXEC

              MOVE SQLCODE                TO ENT-SQLCODE

             EVALUATE SQLCODE
               WHEN +0
                 CONTINUE
               WHEN +100
                 MOVE 'NO VENDOR NAME FOUND' TO ENT-NAME-KEY
               WHEN OTHER
                  GO TO Z9999-RETURN-DB2-ABEND
              END-EVALUATE

              MOVE ENT-NAME-KEY  TO INQPO3-VNDR-NAME
           END-IF.

           IF POH-FK-VDR-ENTPSN-IND = 'P'

              MOVE 'S7000-GET-VNDR-NAME'  TO WSLG-PARAGRAPH
              MOVE   7050                 TO WSLG-SQL-NBR
              MOVE 'SELECT'               TO WSLG-DB2-STATEMENT
              MOVE 'PRS_PRSN_V'           TO WSLG-DB2-TABLE

              EXEC SQL
                 SELECT PRS.NAME_KEY
                   INTO :PRS-NAME-KEY
                   FROM PRS_PRSN_V  PRS
                  WHERE PRS.IREF_ID = :POH-FK-VDR-IREF-ID
              END-EXEC

              MOVE SQLCODE                TO PRS-SQLCODE

             EVALUATE SQLCODE
               WHEN +0
                 CONTINUE
               WHEN +100
                 MOVE 'NO VENDOR NAME FOUND' TO PRS-NAME-KEY
               WHEN OTHER
                  GO TO Z9999-RETURN-DB2-ABEND
              END-EVALUATE

              MOVE PRS-NAME-KEY  TO INQPO3-VNDR-NAME
           END-IF.

      /
       Z9998-LOG-ERROR-VIA-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W TO LOG 2 ERROR MESSAGES TO THE WSLG
      *    TRANSIENT DATA QUEUE AND, IF REQUESTED, TO THE Z/OS CONSOLE.
      *    THE FIRST MESSAGE (SEQ# 0) CONTAINS DESCRIPTIVE ERROR TEXT.
      *    THE SECOND MESSAGE (SEQ# 1) CONTAINS DIAGNOSTIC INFORMATION.
      *    ROLLBACK ANY DB2 UPDATES THAT MAY HAVE OCCURRED THIS TASK.
      ******************************************************************

           MOVE '0'                    TO WSLG-MSG-SEQ-NUM.
           MOVE 'WPU012I'              TO WSLG-PROGRAM-NAME.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           MOVE '1'                    TO WSLG-MSG-SEQ-NUM.
           MOVE EIBTASKN               TO WSLG-CICS-TASK-NUMBER.
           MOVE WSLG-DIAGNOSTIC-AREA   TO WSLG-MSG-TEXT.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           IF WS-UPDATE-COUNT > 0
               MOVE 0                  TO WS-UPDATE-COUNT
               EXEC CICS SYNCPOINT ROLLBACK
               END-EXEC
           END-IF.


       Z9998-LINK-TO-PROGRAM-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W.  IF THE LINK FAILS,
      *    ABEND THE TASK WITH A 'WCIX' DUMP CODE.
      ******************************************************************

           EXEC CICS LINK
                PROGRAM (WSLG-PROGRAM-UIS000W)
                COMMAREA(WSLG-USING-LIST)
                RESP    (WSLG-CICS-RESP)
                RESP2   (WSLG-CICS-RESP2)
           END-EXEC.

           IF WSLG-CICS-RESP NOT = DFHRESP(NORMAL)
               EXEC CICS ABEND
                    ABCODE('WCIX')
               END-EXEC
           END-IF.
      /
       Z9999-RETURN-DATA-ERROR.
      ******************************************************************
      *    RETURN THE DATA ERROR MESSAGE TO THE CALLING PROGRAM.
      *    ROLLBACK ANY DB2 UPDATES THAT MAY HAVE OCCURRED THIS TASK.
      ******************************************************************

           SET INQPO3-RESPONSE-ERROR TO TRUE.
           MOVE 'E'                    TO INQPO3-RETURN-STATUS-CODE.
           MOVE  1                     TO INQPO3-RETURN-MSG-COUNT.

           IF WS-UPDATE-COUNT > 0
               MOVE 0                  TO WS-UPDATE-COUNT
               EXEC CICS SYNCPOINT ROLLBACK
               END-EXEC
           END-IF.

           GO TO Z9999-RETURN-TO-CALLING-PGM.
      /
       Z9999-RETURN-DB2-ABEND.
      ******************************************************************
      *    LOG THE FATAL DB2 ERROR VIA PROGRAM UIS000W
      *    AND ABEND THE TASK WITH A 'WSQL' DUMP CODE.
      ******************************************************************

           MOVE WSLG-FATAL-DB2-MSG-ID  TO WSLG-MSG-ID.
           MOVE WSLG-DB2-STATEMENT     TO WSLG-FATAL-DB2-STATEMENT.
           MOVE WSLG-DB2-TABLE         TO WSLG-FATAL-DB2-TABLE.
           MOVE WSLG-FATAL-DB2-MSG-TEXT
                                       TO WSLG-MSG-TEXT.
           MOVE 'Y'                    TO WSLG-CONSOLE-MSG-IND.
           MOVE SPACES                 TO WSLG-DIAGNOSTIC-INFO.
           MOVE SQLCODE                TO WSLG-SQL-CODE.
           STRING 'SQL#: '                WSLG-SQL-NBR
                  '  SQLCODE: '           WSLG-SQL-CODE
                   DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                     INTO WSLG-DIAGNOSTIC-INFO.
           PERFORM Z9998-LOG-ERROR-VIA-UIS000W.

           EXEC CICS ABEND
                ABCODE('WSQL')
           END-EXEC.
      /
       Z9999-RETURN-SUCCESSFUL.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM WITH SUCCESSFUL STATUS.
      ******************************************************************

           SET INQPO3-RESPONSE-SUCCESSFUL
                                       TO TRUE.
           MOVE 'S'                    TO INQPO3-RETURN-STATUS-CODE.
           MOVE  1                     TO INQPO3-RETURN-MSG-COUNT.
           MOVE SPACES                 TO INQPO3-RETURN-MSG-NUM.
           MOVE SPACES                 TO INQPO3-RETURN-MSG-FIELD.
           MOVE WSLG-SUCCESSFUL-MSG-TEXT
                                       TO INQPO3-RETURN-MSG-TEXT.

           GO TO Z9999-RETURN-TO-CALLING-PGM.


       Z9999-RETURN-TO-CALLING-PGM.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM.
      ******************************************************************

           MOVE INQPO3-PARM-AREA       TO DFHCOMMAREA (1:LENGTH OF
                INQPO3-PARM-AREA).

           EXEC CICS RETURN
           END-EXEC.
