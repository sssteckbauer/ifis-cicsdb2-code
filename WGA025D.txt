       IDENTIFICATION DIVISION.
       PROGRAM-ID.    WGA025D.
      ******************************************************************
      *    PGM/CHANNEL ID .. DELJVCH
      *    IVORY PROJECT ... Web-IFIS
      *    WEB SERVICE ..... DeleteJvoucherDetails
      *    TITLE ........... Delete Jvoucher Detail Transactions
      *    LANGUAGE ........ COBOL Z/OS, DB2, CICS
      *    AUTHOR .......... N. Penalosa
      *    DATE-WRITTEN .... November, 2012
      *
      *                      DESCRIPTION
      *                      -----------
      *    THIS WEB SERVICE WILL DELETE JLD ROWS FOR A SPECIPIC DOC.
      *
      *    DB2 TABLE(S)      HOW ACCESSED
      *    ----------------  -----------------------------------------
      *    JLH               SELECT
      *    JLD               DELETE
      *
      ******************************************************************
      *                      MAINTENANCE HISTORY
      ******************************************************************
      *    MODIFIED BY .....
      *    DATE MODIFIED ...
      *    MODIFICATION ....
      ******************************************************************

           EJECT
       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
       01  FILLER                      PIC X(40)       VALUE
               'WGA025D WORKING-STORAGE BEGINS HERE     '.

       01  WS-MISCELLANEOUS.
           05  WS-SQLCODE              PIC  9(6)       VALUE ZEROES.
           05  WS-UPDATE-COUNT         PIC S9(4)       COMP
                                                       VALUE ZEROES.

       01  WSLG-USING-LIST.
           05  WSLG-CONSOLE-MSG-IND    PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-ID             PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-DATE           PIC X(10)       VALUE SPACES.
           05  WSLG-MSG-TIME           PIC X(08)       VALUE SPACES.
           05  WSLG-PROGRAM-NAME       PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-SEQ-NUM        PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-TEXT           PIC X(80)       VALUE SPACES.

       01  WSLG-DIAGNOSTIC-AREA.
           05  FILLER                  PIC X(05)       VALUE 'PARA:'.
           05  WSLG-PARAGRAPH          PIC X(30)       VALUE SPACES.
           05  FILLER                  PIC X(05)       VALUE 'TASK:'.
           05  WSLG-CICS-TASK-NUMBER   PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  WSLG-DIAGNOSTIC-INFO    PIC X(30)       VALUE SPACES.

       01  WSLG-MISC-AREA.
           05  WSLG-PROGRAM-UIS000W    PIC X(08)       VALUE 'UIS000W '.
           05  WSLG-CICS-RESP          PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-CICS-RESP2         PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-EIBCALEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-CHANLLEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-DB2-STATEMENT      PIC X(08)       VALUE SPACES.
           05  WSLG-DB2-TABLE          PIC X(16)       VALUE SPACES.
           05  WSLG-SQL-NBR            PIC 9(04)       VALUE ZEROES.
           05  WSLG-SQL-CODE           PIC +++++9      VALUE ZEROES.

       01  WSLG-MESSAGE-AREA.
           05  WSLG-SUCCESSFUL-MSG-ID  PIC X(08)       VALUE 'WFIS000I'.
           05  WSLG-SUCCESSFUL-MSG-TEXT
                                       PIC X(40)       VALUE
               'The request was successfully completed. '.

           05  WSLG-CHNL-ID-MSG-ID     PIC X(08)       VALUE 'WFIS001E'.
           05  WSLG-CHNL-ID-MSG-TEXT   PIC X(80)       VALUE
               'The received channel ID value is unrecognized.'.

           05  WSLG-CHNL-VERS-MSG-ID   PIC X(08)       VALUE 'WFIS002E'.
           05  WSLG-CHNL-VERS-MSG-TEXT PIC X(80)       VALUE
               'The received channel version value is unrecognized.'.

           05  WSLG-CHNL-LGTH-MSG-ID   PIC X(08)       VALUE 'WFIS003E'.
           05  WSLG-CHNL-LGTH-MSG-TEXT PIC X(80)       VALUE
               'The received channel length is not equal to the defined
      -        'channel version length. '.

           05  WSLG-FATAL-DB2-MSG-ID   PIC X(08)       VALUE 'WFIS004S'.
           05  WSLG-FATAL-DB2-MSG-TEXT.
               10  FILLER              PIC X(49)       VALUE
                   'DB2 returned a severe SQL error on an attempt to '.
               10  WSLG-FATAL-DB2-STATEMENT
                                       PIC X(08)       VALUE SPACES.
               10  FILLER              PIC X(07)       VALUE ' table '.
               10  WSLG-FATAL-DB2-TABLE
                                       PIC X(16)       VALUE SPACES.
           EJECT
      ******************************************************************
      *01  WGA025P-PARM-AREA.
      ******************************************************************
           COPY WGA025P.

           EJECT
      ******************************************************************
      *    SQL COMMAREA
      ******************************************************************
           EXEC SQL
               INCLUDE SQLCA
           END-EXEC.

           EJECT
      ******************************************************************
      *    JLH_JRNL_HDR_V
      ******************************************************************
           EXEC SQL
               INCLUDE HVJLH
           END-EXEC.

           EJECT
      ******************************************************************
      *    JLD_JRNL_DTL_V
      ******************************************************************
           EXEC SQL
               INCLUDE HVJLD
           END-EXEC.

           EJECT
       LINKAGE SECTION.

       01  DFHCOMMAREA.
           05  COMM-CHANNEL-HEADER.
               10  COMM-CHANNEL-ID     PIC X(08).
                   88  COMM-CHANNEL-ID-OK              VALUE 'DELJVCH '.
               10  COMM-VERS-RESP-CD   PIC X(08).
                   88  COMM-VERSION-01-00-00           VALUE '01.00.00'.
           05  COMM-WGA025D-INPUT-PARMS
                                       PIC X(0086).

           EJECT
       PROCEDURE DIVISION.

       A0000-MAINLINE-ROUTINE.
      ******************************************************************
      *    PERFORM ROUTINE TO EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      *    PERFORM THE REQUESTED FUNCTION AND RETURN CONTROL WHEN
      *    COMPLETED SUCCESSFULLY.
      ******************************************************************

           MOVE 'A0000-MAINLINE-ROUTINE'
                                       TO WSLG-PARAGRAPH.

           PERFORM A0500-EDIT-COMMAREA-VERSION THRU A0500-EXIT.

           MOVE DFHCOMMAREA            TO WGA025P-PARM-AREA.

           INITIALIZE  WGA025P-RETURN-STATUS-PARMS.

           PERFORM A0600-INSPECT-LOWERCASE THRU A0600-EXIT.

           PERFORM A1000-JLH-ROUTINE   THRU A1000-EXIT.

       A0000-CONT.
           GO TO Z9998-RETURN-SUCCESSFUL.
       A0000-EXIT.
           EXIT.


       A0500-EDIT-COMMAREA-VERSION.
      ******************************************************************
      *    EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      ******************************************************************

           MOVE 'A0500-EDIT-COMMAREA-VERSION'
                                       TO WSLG-PARAGRAPH.
           INSPECT COMM-CHANNEL-ID
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           INITIALIZE                     WGA025P-PARM-AREA.
           MOVE COMM-CHANNEL-HEADER    TO WGA025P-CHANNEL-HEADER.

           IF COMM-CHANNEL-ID-OK
               EVALUATE TRUE
               WHEN COMM-VERSION-01-00-00

                   IF EIBCALEN = LENGTH OF WGA025P-PARM-AREA
                       NEXT SENTENCE
                   ELSE
                       SET WGA025P-RESPONSE-ERROR
                                       TO TRUE
                       MOVE 'E'        TO WGA025P-RETURN-STATUS-CODE    E
                       MOVE  1         TO WGA025P-RETURN-MSG-COUNT
                       MOVE 'A05001'   TO WGA025P-RETURN-MSG-NUM
                       MOVE 'Channel-Id'
                                       TO WGA025P-RETURN-MSG-FIELD
                       MOVE 'The channel length is invalid           '
                                       TO WGA025P-RETURN-MSG-TEXT
                       MOVE WSLG-CHNL-LGTH-MSG-ID
                                       TO WSLG-MSG-ID
                       MOVE WSLG-CHNL-LGTH-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                       MOVE 'N'        TO WSLG-CONSOLE-MSG-IND
                       MOVE SPACES     TO WSLG-DIAGNOSTIC-INFO
                       MOVE EIBCALEN   TO WSLG-EIBCALEN-NUMEDIT
                       MOVE LENGTH OF WGA025P-PARM-AREA
                                       TO WSLG-CHANLLEN-NUMEDIT
                       STRING 'EIBCALEN ' WSLG-EIBCALEN-NUMEDIT
                              ' versus '  WSLG-CHANLLEN-NUMEDIT
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                       PERFORM Z9994-LOG-ERROR-VIA-UIS000W
                          THRU Z9994-EXIT
                       GO TO Z9999-RETURN-TO-CALLING-PGM
                   END-IF

               WHEN OTHER
                   SET WGA025P-RESPONSE-ERROR
                                       TO TRUE
                   MOVE 'E'            TO WGA025P-RETURN-STATUS-CODE
                   MOVE  1             TO WGA025P-RETURN-MSG-COUNT
                   MOVE 'A05002'       TO WGA025P-RETURN-MSG-NUM
                   MOVE 'Channel-Id'   TO WGA025P-RETURN-MSG-FIELD
                   MOVE 'The channel version is invalid          '
                                       TO WGA025P-RETURN-MSG-TEXT
                   MOVE WSLG-CHNL-VERS-MSG-ID
                                       TO WSLG-MSG-ID
                   MOVE WSLG-CHNL-VERS-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                   MOVE 'N'            TO WSLG-CONSOLE-MSG-IND
                   MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
                   STRING 'Received version: ' COMM-VERS-RESP-CD
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                   PERFORM Z9994-LOG-ERROR-VIA-UIS000W
                      THRU Z9994-EXIT
                   GO TO Z9999-RETURN-TO-CALLING-PGM
               END-EVALUATE
           ELSE
               SET WGA025P-RESPONSE-ERROR
                                       TO TRUE
               MOVE 'E'                TO WGA025P-RETURN-STATUS-CODE
               MOVE  1                 TO WGA025P-RETURN-MSG-COUNT
               MOVE 'A05003'           TO WGA025P-RETURN-MSG-NUM
               MOVE 'Channel-Id'       TO WGA025P-RETURN-MSG-FIELD
               MOVE 'The channel id value is invalid         '
                                       TO WGA025P-RETURN-MSG-TEXT
               MOVE WSLG-CHNL-ID-MSG-ID
                                       TO WSLG-MSG-ID
               MOVE WSLG-CHNL-ID-MSG-TEXT
                                       TO WSLG-MSG-TEXT
               MOVE 'N'                TO WSLG-CONSOLE-MSG-IND
               MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
               STRING 'Received Channel Id: ' COMM-CHANNEL-ID
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
               PERFORM Z9994-LOG-ERROR-VIA-UIS000W THRU Z9994-EXIT
               GO TO Z9999-RETURN-TO-CALLING-PGM
           END-IF.

       A0500-EXIT.
           EXIT.

       A0600-INSPECT-LOWERCASE.

           INSPECT WGA025P-DOC-NUMBER
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

       A0600-EXIT.
           EXIT.

       EJECT
       A1000-JLH-ROUTINE.
      ******************************************************************
      *    PROCESS DOCUMENT NUMBER HEADER
      ******************************************************************

           MOVE 'A1000-JLH-ROUTINE'    TO WSLG-PARAGRAPH.
           MOVE WGA025P-DOC-NUMBER     TO JLH-DOC-NBR.

           EXEC SQL
               SELECT  DOC_NBR
                 INTO :JLH-DOC-NBR
                 FROM  JLH_JRNL_HDR_V
                WHERE  DOC_NBR = :JLH-DOC-NBR
                  AND  APRVL_IND <> 'Y'
           END-EXEC.

           IF SQLCODE = +0
               CONTINUE
           ELSE
               IF SQLCODE = +100
                   MOVE 'A20001'           TO WGA025P-RETURN-MSG-NUM
                   MOVE 'Document-number'  TO WGA025P-RETURN-MSG-FIELD
                   MOVE 'Doc not found or already approved'
                                           TO WGA025P-RETURN-MSG-TEXT
                   GO TO Z9996-RETURN-DATA-ERROR
               ELSE
                   MOVE SQLCODE            TO WS-SQLCODE
                   MOVE WS-SQLCODE         TO WGA025P-RETURN-MSG-NUM
                   MOVE 'DB2 '             TO WGA025P-RETURN-MSG-FIELD
                   MOVE 'Select error on JLH      '
                                           TO WGA025P-RETURN-MSG-TEXT
                   GO TO Z9996-RETURN-DATA-ERROR
               END-IF
           END-IF.

      ******************************************************************
      *    DELETE DETAILS FROM JLD_JRNL_DTL_V.
      ******************************************************************


           MOVE   4000                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'JLD'                  TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE FROM JLD_JRNL_DTL_V
               WHERE FK_JLH_DOC_NBR = :JLH-DOC-NBR
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD +1 TO WS-UPDATE-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               MOVE SQLCODE            TO WS-SQLCODE
               MOVE WS-SQLCODE         TO WGA025P-RETURN-MSG-NUM
               MOVE 'DB2 '             TO WGA025P-RETURN-MSG-FIELD
               MOVE 'Delete error from JLD    '
                                       TO WGA025P-RETURN-MSG-TEXT
               GO TO Z9996-RETURN-DATA-ERROR
           END-EVALUATE.

      ******************************************************************
      *    DELETE JV HEADER FROM JLH_JRNL_HDR_V.
      ******************************************************************


           MOVE   4001                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'JLH'                  TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE FROM JLH_JRNL_HDR_V
                WHERE  DOC_NBR = :JLH-DOC-NBR
                  AND  APRVL_IND <> 'Y'
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD +1 TO WS-UPDATE-COUNT
             WHEN OTHER
               MOVE SQLCODE            TO WS-SQLCODE
               MOVE WS-SQLCODE         TO WGA025P-RETURN-MSG-NUM
               MOVE 'DB2 '             TO WGA025P-RETURN-MSG-FIELD
               MOVE 'Delete error from JLH    '
                                       TO WGA025P-RETURN-MSG-TEXT
               GO TO Z9996-RETURN-DATA-ERROR
           END-EVALUATE.

       A1000-EXIT.
           EXIT.

           EJECT
       Z9994-LOG-ERROR-VIA-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W TO LOG 2 ERROR MESSAGES TO THE WSLG
      *    TRANSIENT DATA QUEUE AND, IF REQUESTED, TO THE Z/OS CONSOLE.
      *    THE FIRST MESSAGE (SEQ# 0) CONTAINS DESCRIPTIVE ERROR TEXT.
      *    THE SECOND MESSAGE (SEQ# 1) CONTAINS DIAGNOSTIC INFORMATION.
      *    ROLLBACK ANY DB2 UPDATES THAT MAY HAVE OCCURRED THIS TASK.
      ******************************************************************

           MOVE '0'                    TO WSLG-MSG-SEQ-NUM.
           MOVE 'WGA025D'              TO WSLG-PROGRAM-NAME.
           PERFORM Z9995-LINK-TO-PROGRAM-UIS000W THRU Z9995-EXIT.

           MOVE '1'                    TO WSLG-MSG-SEQ-NUM.
           MOVE EIBTASKN               TO WSLG-CICS-TASK-NUMBER.
           MOVE WSLG-DIAGNOSTIC-AREA   TO WSLG-MSG-TEXT.
           PERFORM Z9995-LINK-TO-PROGRAM-UIS000W THRU Z9995-EXIT.

           IF WS-UPDATE-COUNT > 0
               MOVE 0                  TO WS-UPDATE-COUNT
               EXEC CICS SYNCPOINT ROLLBACK
               END-EXEC
           END-IF.

       Z9994-EXIT.
           EXIT.

       Z9995-LINK-TO-PROGRAM-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W.  IF THE LINK FAILS,
      *    ABEND THE TASK WITH A 'WCIX' DUMP CODE.
      ******************************************************************

           EXEC CICS LINK
                PROGRAM (WSLG-PROGRAM-UIS000W)
                COMMAREA(WSLG-USING-LIST)
                RESP    (WSLG-CICS-RESP)
                RESP2   (WSLG-CICS-RESP2)
           END-EXEC.

           IF WSLG-CICS-RESP NOT = DFHRESP(NORMAL)
               EXEC CICS ABEND
                    ABCODE('WCIX')
               END-EXEC
           END-IF.

       Z9995-EXIT.
           EXIT.

       Z9996-RETURN-DATA-ERROR.
      ******************************************************************
      *    RETURN THE DATA ERROR MESSAGE TO THE CALLING PROGRAM.
      *    ROLLBACK ANY DB2 UPDATES THAT MAY HAVE OCCURRED THIS TASK.
      ******************************************************************

           SET WGA025P-RESPONSE-ERROR TO TRUE.
           MOVE 'E'                    TO WGA025P-RETURN-STATUS-CODE.
           MOVE  1                     TO WGA025P-RETURN-MSG-COUNT.

           IF WS-UPDATE-COUNT > 0
               MOVE 0                  TO WS-UPDATE-COUNT
               EXEC CICS SYNCPOINT ROLLBACK
               END-EXEC
           END-IF.

           GO TO Z9999-RETURN-TO-CALLING-PGM.

       Z9996-EXIT.
           EXIT.

       Z9997-RETURN-DB2-ABEND.
      ******************************************************************
      *    LOG THE FATAL DB2 ERROR VIA PROGRAM UIS000W
      *    AND ABEND THE TASK WITH A 'WSQL' DUMP CODE.
      ******************************************************************

           MOVE WSLG-FATAL-DB2-MSG-ID  TO WSLG-MSG-ID.
           MOVE WSLG-DB2-STATEMENT     TO WSLG-FATAL-DB2-STATEMENT.
           MOVE WSLG-DB2-TABLE         TO WSLG-FATAL-DB2-TABLE.
           MOVE WSLG-FATAL-DB2-MSG-TEXT
                                       TO WSLG-MSG-TEXT.
           MOVE 'Y'                    TO WSLG-CONSOLE-MSG-IND.
           MOVE SPACES                 TO WSLG-DIAGNOSTIC-INFO.
           MOVE SQLCODE                TO WSLG-SQL-CODE.
           STRING 'SQL#: '                WSLG-SQL-NBR
                  '  SQLCODE: '           WSLG-SQL-CODE
                   DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                     INTO WSLG-DIAGNOSTIC-INFO.
           PERFORM Z9994-LOG-ERROR-VIA-UIS000W THRU Z9994-EXIT.

           EXEC CICS ABEND
                ABCODE('WSQL')
           END-EXEC.

       Z9997-EXIT.
           EXIT.

       Z9998-RETURN-SUCCESSFUL.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM WITH SUCCESSFUL STATUS.
      ******************************************************************

           SET WGA025P-RESPONSE-SUCCESSFUL
                                       TO TRUE.
           MOVE 'S'                    TO WGA025P-RETURN-STATUS-CODE.
           MOVE  1                     TO WGA025P-RETURN-MSG-COUNT.
           IF  SQLCODE = +0
               MOVE SPACES             TO WGA025P-RETURN-MSG-NUM
               MOVE SPACES             TO WGA025P-RETURN-MSG-FIELD
               MOVE WSLG-SUCCESSFUL-MSG-TEXT
                                       TO WGA025P-RETURN-MSG-TEXT
           END-IF.

           GO TO Z9999-RETURN-TO-CALLING-PGM.

       Z9998-EXIT.
           EXIT.

       Z9999-RETURN-TO-CALLING-PGM.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM.
      ******************************************************************

           MOVE WGA025P-PARM-AREA      TO DFHCOMMAREA.

           EXEC CICS RETURN
           END-EXEC.

       Z9999-EXIT.
           EXIT.
