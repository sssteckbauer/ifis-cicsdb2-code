       IDENTIFICATION DIVISION.
       PROGRAM-ID.    WUT003U.
      ******************************************************************
      *    TITLE ........... UPDATE APH APPROVAL LEVEL FOR ANY DOCUMENT
      *                     (UPDAPRVL)
      *    LANGUAGE ........ COBOL Z/OS, DB2, CICS SUBROUTINE
      *    AUTHOR .......... H. VANDERWEIT
      *    DATE-WRITTEN .... MAY, 2009
      *
      *                      DESCRIPTION
      *                      -----------
      *    THIS SUBROUTINE IS DYNAMICALLY CALLED FROM EITHER A WEB
      *    SERVICE (I.E. WGA015L/JVAPRVL) OR A MAINFRAME CICS PROGRAM.
      *    THE DOCUMENT NUMBER, SEQUENCE NUMBER, FISCAL YEAR, CHANGE
      *    SEQUENCE NUMBER, APPROVAL ID, AND DOCUMENT AMOUNT ARE
      *    RECEIVED IN A COMMAREA TO DETERMINE THE APPROVAL LEVEL
      *    AND UPDATE THE APH_APRVL_HDR AND APD_APRVL_AUD TABLES
      *    ACCORDINGLY.  A STATUS CODE IS RETURNED TO THE CALLING
      *    PROGRAM TO INDICATE EITHER A SUCCESSFUL APPROVAL OR AN
      *    ERROR CONDITION WITH A DESCRIPTIVE MESSAGE.  FOR SUCCESSFUL
      *    APPROVALS, THE UPDAPRVL-RETURN-MSG-FIELD IS SET TO EITHER
      *    'LEVEL APPROVAL' OR 'FINAL APPROVAL'.
      *
      *    DB2 TABLE(S)      HOW ACCESSED
      *    ----------------  -----------------------------------------
      *    APA_APRVL_ALT_V   SELECT
      *    APD_APRVL_AUD_V   SELECT, UPDATE, INSERT
      *    APH_APRVL_HDR_V   SELECT, UPDATE
      *    APL_APRVL_LVL_V   SELECT
      *
      *    CALLED PROGRAM(S) FUNCTION
      *    ----------------  -----------------------------------------
      *    UIS000W           LOG ERRORS TO WSLG TRANSIENT DATA QUEUE
      *
      ******************************************************************
      *                      MAINTENANCE HISTORY                       *
      ******************************************************************
      *    MODIFIED BY .....
      *    DATE MODIFIED ...
      *    MODIFICATION ....
      ******************************************************************
      /
       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
       01  FILLER                      PIC X(36)       VALUE
           'WUT003U WORKING-STORAGE BEGINS HERE'.

       01  WS-MISCELLANEOUS.
           05  WS-SQLCOUNT             PIC S9(8) COMP  VALUE ZEROES.
           05  WS-UPDATE-COUNT         PIC S9(4) COMP  VALUE ZEROES.

       01  WSLG-USING-LIST.
           05  WSLG-CONSOLE-MSG-IND    PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-ID             PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-DATE           PIC X(10)       VALUE SPACES.
           05  WSLG-MSG-TIME           PIC X(08)       VALUE SPACES.
           05  WSLG-PROGRAM-NAME       PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-SEQ-NUM        PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-TEXT           PIC X(80)       VALUE SPACES.

       01  WSLG-DIAGNOSTIC-AREA.
           05  FILLER                  PIC X(05)       VALUE 'PARA:'.
           05  WSLG-PARAGRAPH          PIC X(30)       VALUE SPACES.
           05  FILLER                  PIC X(05)       VALUE 'TASK:'.
           05  WSLG-CICS-TASK-NUMBER   PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  WSLG-DIAGNOSTIC-INFO    PIC X(30)       VALUE SPACES.

       01  WSLG-MISC-AREA.
           05  WSLG-PROGRAM-UIS000W    PIC X(08)       VALUE 'UIS000W '.
           05  WSLG-CICS-RESP          PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-CICS-RESP2         PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-EIBCALEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-CHANLLEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-DB2-STATEMENT      PIC X(08)       VALUE SPACES.
           05  WSLG-DB2-TABLE          PIC X(16)       VALUE SPACES.
           05  WSLG-SQL-NBR            PIC 9(04)       VALUE ZEROES.
           05  WSLG-SQL-CODE           PIC +++++9      VALUE ZEROES.

       01  WSLG-MESSAGE-AREA.
           05  WSLG-SUCCESSFUL-MSG-ID  PIC X(08)       VALUE 'WFUT000I'.
           05  WSLG-SUCCESSFUL-MSG-TEXT
                                       PIC X(40)       VALUE
               'The request was successfully completed. '.

           05  WSLG-CHNL-ID-MSG-ID     PIC X(08)       VALUE 'WFUT001E'.
           05  WSLG-CHNL-ID-MSG-TEXT   PIC X(80)       VALUE
               'The received channel ID value is unrecognized.'.

           05  WSLG-CHNL-VERS-MSG-ID   PIC X(08)       VALUE 'WFUT002E'.
           05  WSLG-CHNL-VERS-MSG-TEXT PIC X(80)       VALUE
               'The received channel version value is unrecognized.'.

           05  WSLG-CHNL-LGTH-MSG-ID   PIC X(08)       VALUE 'WFUT003E'.
           05  WSLG-CHNL-LGTH-MSG-TEXT PIC X(80)       VALUE
               'The received channel length is not equal to the defined
      -        'channel version length. '.

           05  WSLG-FATAL-DB2-MSG-ID   PIC X(08)       VALUE 'WFUT004S'.
           05  WSLG-FATAL-DB2-MSG-TEXT.
               10  FILLER              PIC X(49)       VALUE
                   'DB2 returned a severe SQL error on an attempt to '.
               10  WSLG-FATAL-DB2-MSG-FIELDS.
                   15  WSLG-FATAL-DB2-STATEMENT
                                       PIC X(08)       VALUE SPACES.
                   15  FILLER          PIC X(06)       VALUE 'table '.
                   15  WSLG-FATAL-DB2-TABLE
                                       PIC X(16)       VALUE SPACES.
      /
      ******************************************************************
      *    SQL COMMAREA
      ******************************************************************
           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
      ******************************************************************
      *    APA_APRVL_ALT_V
      ******************************************************************
       01  APA-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'APA*'.
           05  APA-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVAPA
           END-EXEC.
      /
      ******************************************************************
      *    APD_APRVL_AUD_V
      ******************************************************************
       01  APD-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'APD*'.
           05  APD-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVAPD
           END-EXEC.
      /
      ******************************************************************
      *    APH_APRVL_HDR_V
      ******************************************************************
       01  APH-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'APH*'.
           05  APH-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVAPH
           END-EXEC.
      /
      ******************************************************************
      *    APL_APRVL_LVL_V
      ******************************************************************
       01  APL-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'APL*'.
           05  APL-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVAPL
           END-EXEC.
      /
       LINKAGE SECTION.
      /
      ******************************************************************
      *01  UPDAPRVL-PARM-AREA.
      ******************************************************************
           COPY WUT003U.

       PROCEDURE DIVISION USING UPDAPRVL-PARM-AREA.

      /
       A0000-MAINLINE-ROUTINE.
      ******************************************************************
      *    PERFORM ROUTINE TO DETERMINE AND UPDATE THE APPROVAL LEVEL.
      ******************************************************************

           MOVE 'A0000-MAINLINE-ROUTINE'   TO WSLG-PARAGRAPH.

           INITIALIZE UPDAPRVL-RETURN-STATUS-PARMS.

           IF UPDAPRVL-CHANNEL-ID NOT = 'UPDAPRVL'
               MOVE 'A00001'               TO UPDAPRVL-RETURN-MSG-NUM
               MOVE 'CHANNEL-ID'           TO UPDAPRVL-RETURN-MSG-FIELD
               MOVE 'The UPDAPRVL Channel Id is invalid       '
                                           TO UPDAPRVL-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           PERFORM A4000-APPROVAL-ROUTINE.

           GO TO Z9999-RETURN-SUCCESSFUL.
      /
       A4000-APPROVAL-ROUTINE.
      ******************************************************************
      * => DO THE FOLLOWING TO DETERMINE AND UPDATE THE APPROVAL LEVEL:
      *    GET THE APH APPROVAL HEADER ROW FOR THE PASSED DOCUMENT.
      *    GET THE APL APPROVAL LEVEL ROW USING THE APH FOREIGN KEY.
      *    IF THE APL APPROVAL ID DOESN'T MATCH THE PASSED APPROVAL ID,
      *        CHECK IF THE PASSED ID IS FOR AN ALTERNATE APPROVER.
      *    GET THE APD APPROVAL AUDIT ROW FOR THE PASSED DOCUMENT AND
      *        APL APPROVAL LEVEL.  IF NOT FOUND, CREATE/INSERT IT.
      *        IF FOUND, UPDATE IT WITH THE PASSED APPROVAL ID.
      *    IF THE APPROVER'S APL APPROVAL LIMIT MATCHES OR EXCEEDS THE
      *        PASSED DOCUMENT AMOUNT, UPDATE THE APH APPROVAL HEADER
      *        TO "FINAL APPROVAL" STATUS AND EXIT SUCCESSFULLY.
      *    IF NOT AT THE 0001 APL APPROVAL LEVEL, GET THE NEXT (MINUS 1)
      *        LEVEL, UPDATE THE APH APPROVAL HEADER'S FOREIGN KEY
      *        TO THE NEXT LEVEL APPROVER, AND EXIT SUCCESSFULLY.
      ******************************************************************

           MOVE 'A4000-APPROVAL-ROUTINE'
                                       TO WSLG-PARAGRAPH.

           IF UPDAPRVL-DOC-AMT > 0
               CONTINUE
           ELSE
               MOVE 'A40001'           TO UPDAPRVL-RETURN-MSG-NUM
               MOVE 'DOC-AMT'          TO UPDAPRVL-RETURN-MSG-FIELD
               MOVE 'Document amt must be greater than zero  '
                                       TO UPDAPRVL-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           MOVE UPDAPRVL-DOC-SEQ-NBR   TO APH-SEQ-NBR.
           MOVE UPDAPRVL-FSCL-YR       TO APH-FSCL-YR.
           MOVE UPDAPRVL-DOC-NBR       TO APH-DOC-NBR.
           MOVE UPDAPRVL-CHNG-SEQ-NBR  TO APH-CHNG-SEQ-NBR.
           PERFORM S4000-SELECT-APH.

           IF  APH-SQLCODE  = +0
           AND APH-APL-SETF = 'Y'
               CONTINUE
           ELSE
               MOVE 'A40002'           TO UPDAPRVL-RETURN-MSG-NUM
               MOVE 'SEQ-NBR/FSCL-YR/DOC-NBR/CHGSEQ'
                                       TO UPDAPRVL-RETURN-MSG-FIELD
               MOVE 'The APH Approval Header does not exist  '
                                       TO UPDAPRVL-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           MOVE APH-FK-APL-SEQ-NBR     TO APL-FK-APV-SEQ-NBR.
           MOVE APH-FK-APL-USER-ID     TO APL-FK-APV-USER-ID.
           MOVE APH-FK-APL-APV-TPLT-CD TO APL-FK-APV-APV-TPLT-CD.
           MOVE APH-FK-APL-LVL-SEQ-NBR TO APL-LEVEL-SEQ-NBR.
           PERFORM S4100-SELECT-APL.

           IF APL-SQLCODE = +100
               MOVE 'A40003'           TO UPDAPRVL-RETURN-MSG-NUM
               MOVE 'SEQ-NBR/FSCL-YR/DOC-NBR/CHGSEQ'
                                       TO UPDAPRVL-RETURN-MSG-FIELD
               MOVE 'APL Approval Lvl does not exist for APH '
                                       TO UPDAPRVL-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           IF APL-APRVL-ID NOT = UPDAPRVL-APRVL-ID
      *%       *** CHECK IF THE PASSED APRVL-ID IS AN ALTERNATE ***
               MOVE APH-FK-APL-SEQ-NBR TO APA-FK-APL-SEQ-NBR
               MOVE APH-FK-APL-USER-ID TO APA-FK-APL-USER-ID
               MOVE APH-FK-APL-APV-TPLT-CD
                                       TO APA-FK-APL-APV-TPLT-CD
               MOVE APH-FK-APL-LVL-SEQ-NBR
                                       TO APA-FK-APL-LVL-SEQ-NBR
               MOVE UPDAPRVL-APRVL-ID  TO APA-ALTRNT-APRVL-CD
               PERFORM S4200-SELECT-APA-EXISTS

               IF WS-SQLCOUNT > +0
                   CONTINUE
               ELSE
                   MOVE 'A40004'       TO UPDAPRVL-RETURN-MSG-NUM
                   MOVE 'SEQ-NBR/FSCL-YR/DOC-NBR/CHGSEQ'
                                       TO UPDAPRVL-RETURN-MSG-FIELD
                   MOVE 'Approval Id not found as primary or alt '
                                       TO UPDAPRVL-RETURN-MSG-TEXT
                   GO TO Z9999-RETURN-DATA-ERROR
               END-IF
           ELSE
               MOVE SPACES             TO APA-ALTRNT-APRVL-CD
           END-IF.

           MOVE APH-SEQ-NBR            TO APD-FK-APH-SEQ-NBR.
           MOVE APH-FSCL-YR            TO APD-FK-APH-FSCL-YR.
           MOVE APH-DOC-NBR            TO APD-FK-APH-DOC-NBR.
           MOVE APH-CHNG-SEQ-NBR       TO APD-FK-APH-CHG-SEQ-NBR.
           MOVE APL-LEVEL-SEQ-NBR      TO APD-LEVEL-SEQ-NBR.
           PERFORM S4300-SELECT-APD.

           IF APD-SQLCODE = +100
               INITIALIZE                 APD-APD-APRVL-AUD
               MOVE APH-APRVL-ID       TO APD-USER-CD
               MOVE APH-APRVL-ID       TO APD-APRVL-CD
               MOVE APA-ALTRNT-APRVL-CD
                                       TO APD-ALT-APRVL-CD
               MOVE APH-SEQ-NBR        TO APD-FK-APH-SEQ-NBR
               MOVE APH-FSCL-YR        TO APD-FK-APH-FSCL-YR
               MOVE APH-DOC-NBR        TO APD-FK-APH-DOC-NBR
               MOVE APH-CHNG-SEQ-NBR   TO APD-FK-APH-CHG-SEQ-NBR
               MOVE APL-LEVEL-SEQ-NBR  TO APD-LEVEL-SEQ-NBR
               PERFORM S4400-INSERT-APD
           ELSE
               IF  APD-APRVL-CD     = APH-APRVL-ID
               AND APD-ALT-APRVL-CD = APA-ALTRNT-APRVL-CD
                   NEXT SENTENCE
               ELSE
                   MOVE APH-APRVL-ID   TO APD-USER-CD
                   MOVE APH-APRVL-ID   TO APD-APRVL-CD
                   MOVE APA-ALTRNT-APRVL-CD
                                       TO APD-ALT-APRVL-CD
                   PERFORM S4500-UPDATE-APD
               END-IF
           END-IF.

           IF  APL-APRVL-LIMIT >= APH-DOC-AMT
           AND APL-APRVL-LIMIT >= UPDAPRVL-DOC-AMT
               MOVE 'Y'                TO APH-APRVL-IND
               MOVE 'N'                TO APH-IDX-APRVL-SETF
               MOVE 'N'                TO APH-IDX-UNAPRVD-SETF
               MOVE 'N'                TO APH-APL-SETF
               MOVE UPDAPRVL-DOC-AMT   TO APH-DOC-AMT
               PERFORM S4700-UPDATE-APH-FINAL

               MOVE 'A40005'           TO UPDAPRVL-RETURN-MSG-NUM
               MOVE 'FINAL APPROVAL'   TO UPDAPRVL-RETURN-MSG-FIELD
               MOVE 'Final Approval has been received        '
                                       TO UPDAPRVL-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-SUCCESSFUL
           END-IF.

      ******************************************************************
      *    GET THE NEXT (MINUS 1) APL APPROVAL LEVEL AND THEN
      *    SET THE APH_APRVL_HDR FOREIGN KEY TO THE NEXT LEVEL APPROVER.
      ******************************************************************
           IF APL-LEVEL-SEQ-NBR = 0001
               MOVE 'A40006'           TO UPDAPRVL-RETURN-MSG-NUM
               MOVE 'SEQ-NBR/FSCL-YR/DOC-NBR/CHGSEQ'
                                       TO UPDAPRVL-RETURN-MSG-FIELD
               MOVE 'The maximum approval amount is invalid  '
                                       TO UPDAPRVL-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           SUBTRACT 1                FROM APL-LEVEL-SEQ-NBR.
           PERFORM S4100-SELECT-APL.

           IF APL-SQLCODE = +100
               MOVE 'A40007'           TO UPDAPRVL-RETURN-MSG-NUM
               MOVE 'SEQ-NBR/FSCL-YR/DOC-NBR/CHGSEQ'
                                       TO UPDAPRVL-RETURN-MSG-FIELD
               MOVE 'Invalid approval authority              '
                                       TO UPDAPRVL-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           MOVE APL-APRVL-ID           TO APH-APRVL-ID.
           MOVE APL-FK-APV-SEQ-NBR     TO APH-FK-APL-SEQ-NBR.
           MOVE APL-FK-APV-USER-ID     TO APH-FK-APL-USER-ID.
           MOVE APL-FK-APV-APV-TPLT-CD TO APH-FK-APL-APV-TPLT-CD.
           MOVE APL-LEVEL-SEQ-NBR      TO APH-FK-APL-LVL-SEQ-NBR.

           PERFORM S4600-UPDATE-APH-NEXT-LVL.

           MOVE 'A40008'               TO UPDAPRVL-RETURN-MSG-NUM.
           MOVE 'LEVEL APPROVAL'       TO UPDAPRVL-RETURN-MSG-FIELD.
           MOVE 'Document approved for current level     '
                                       TO UPDAPRVL-RETURN-MSG-TEXT.
      /
       S4000-SELECT-APH.
      ******************************************************************
      *    SELECT APH_APRVL_HDR_V.
      ******************************************************************

           MOVE 'S4000-SELECT-APH'         TO WSLG-PARAGRAPH.
           MOVE   4000                     TO WSLG-SQL-NBR.
           MOVE 'SELECT'                   TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT APH.APRVL_ID
                     ,APH.FK_APL_SEQ_NBR
                     ,APH.FK_APL_USER_ID
                     ,APH.FK_APL_APV_TPLT_CD
                     ,APH.FK_APL_LVL_SEQ_NBR
                     ,APH.APL_SETF
                     ,APH.DOC_AMT
                INTO :APH-APRVL-ID
                    ,:APH-FK-APL-SEQ-NBR
                    ,:APH-FK-APL-USER-ID
                    ,:APH-FK-APL-APV-TPLT-CD
                    ,:APH-FK-APL-LVL-SEQ-NBR
                    ,:APH-APL-SETF
                    ,:APH-DOC-AMT
                 FROM APH_APRVL_HDR_V APH
                WHERE APH.SEQ_NBR       = :APH-SEQ-NBR
                  AND APH.FSCL_YR       = :APH-FSCL-YR
                  AND APH.DOC_NBR       = :APH-DOC-NBR
                  AND APH.CHNG_SEQ_NBR  = :APH-CHNG-SEQ-NBR
           END-EXEC.

           MOVE SQLCODE                    TO APH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S4100-SELECT-APL.
      ******************************************************************
      *    SELECT APL_APRVL_LVL_V.
      ******************************************************************

           MOVE 'S4100-SELECT-APL'         TO WSLG-PARAGRAPH.
           MOVE   4100                     TO WSLG-SQL-NBR.
           MOVE 'SELECT'                   TO WSLG-DB2-STATEMENT.
           MOVE 'APL_APRVL_LVL_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT APL.APRVL_ID
                     ,APL.APRVL_LIMIT
                INTO :APL-APRVL-ID
                    ,:APL-APRVL-LIMIT
                 FROM APL_APRVL_LVL_V  APL
                WHERE APL.FK_APV_SEQ_NBR     = :APL-FK-APV-SEQ-NBR
                  AND APL.FK_APV_USER_ID     = :APL-FK-APV-USER-ID
                  AND APL.FK_APV_APV_TPLT_CD = :APL-FK-APV-APV-TPLT-CD
                  AND APL.LEVEL_SEQ_NBR      = :APL-LEVEL-SEQ-NBR
           END-EXEC.

           MOVE SQLCODE                    TO APL-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S4200-SELECT-APA-EXISTS.
      ******************************************************************
      *    SELECT APA_APRVL_ALT_V EXISTS.
      ******************************************************************

           MOVE 'S4200-SELECT-APA-EXISTS'  TO WSLG-PARAGRAPH.
           MOVE   4200                     TO WSLG-SQL-NBR.
           MOVE 'SELECT'                   TO WSLG-DB2-STATEMENT.
           MOVE 'APA_APRVL_ALT_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT COUNT (*)
                 INTO :WS-SQLCOUNT
                 FROM APA_APRVL_ALT_V  APA
                WHERE APA.FK_APL_SEQ_NBR     = :APA-FK-APL-SEQ-NBR
                  AND APA.FK_APL_USER_ID     = :APA-FK-APL-USER-ID
                  AND APA.FK_APL_APV_TPLT_CD = :APA-FK-APL-APV-TPLT-CD
                  AND APA.FK_APL_LVL_SEQ_NBR = :APA-FK-APL-LVL-SEQ-NBR
                  AND APA.ALTRNT_APRVL_CD    = :APA-ALTRNT-APRVL-CD
           END-EXEC.

           MOVE SQLCODE                    TO APA-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S4300-SELECT-APD.
      ******************************************************************
      *    SELECT APD_APRVL_AUD_V.
      ******************************************************************

           MOVE 'S4300-SELECT-APD'         TO WSLG-PARAGRAPH.
           MOVE   4300                     TO WSLG-SQL-NBR.
           MOVE 'SELECT'                   TO WSLG-DB2-STATEMENT.
           MOVE 'APD_APRVL_AUD_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT  APD.APRVL_CD
                      ,APD.ALT_APRVL_CD
                 INTO :APD-APRVL-CD
                     ,:APD-ALT-APRVL-CD
                 FROM  APD_APRVL_AUD_V  APD
                WHERE  APD.FK_APH_FSCL_YR     = :APD-FK-APH-FSCL-YR
                  AND  APD.FK_APH_SEQ_NBR     = :APD-FK-APH-SEQ-NBR
                  AND  APD.FK_APH_DOC_NBR     = :APD-FK-APH-DOC-NBR
                  AND  APD.FK_APH_CHG_SEQ_NBR = :APD-FK-APH-CHG-SEQ-NBR
                  AND  APD.LEVEL_SEQ_NBR      = :APD-LEVEL-SEQ-NBR
           END-EXEC.

           MOVE SQLCODE                    TO APD-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S4400-INSERT-APD.
      ******************************************************************
      *    INSERT APD_APRVL_AUD_V.
      ******************************************************************

           MOVE 'S4400-INSERT-APD'         TO WSLG-PARAGRAPH.
           MOVE   4400                     TO WSLG-SQL-NBR.
           MOVE 'INSERT'                   TO WSLG-DB2-STATEMENT.
           MOVE 'APD_APRVL_AUD_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               INSERT INTO APD_APRVL_AUD_V
                 (USER_CD
                 ,LAST_ACTVY_DT
                 ,LEVEL_SEQ_NBR
                 ,APRVL_CD
                 ,ALT_APRVL_CD
                 ,APRVL_DT
                 ,APRVL_TIME
                 ,SRC_IND
                 ,PAN_IND
                 ,FK_APH_FSCL_YR
                 ,FK_APH_SEQ_NBR
                 ,FK_APH_DOC_NBR
                 ,FK_APH_CHG_SEQ_NBR
                 )
               VALUES
                 (:APD-USER-CD
                 ,:APD-LAST-ACTVY-DT
                 ,:APD-LEVEL-SEQ-NBR
                 ,:APD-APRVL-CD
                 ,:APD-ALT-APRVL-CD
                 , CURRENT_DATE
                 , CURRENT_TIME
                 ,:APD-SRC-IND
                 ,:APD-PAN-IND
                 ,:APD-FK-APH-FSCL-YR
                 ,:APD-FK-APH-SEQ-NBR
                 ,:APD-FK-APH-DOC-NBR
                 ,:APD-FK-APH-CHG-SEQ-NBR
                 )
           END-EXEC.

           MOVE SQLCODE                    TO APD-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                       TO WS-UPDATE-COUNT
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S4500-UPDATE-APD.
      ******************************************************************
      *    UPDATE APD_APRVL_AUD_V.
      ******************************************************************

           MOVE 'S4500-UPDATE-APD'         TO WSLG-PARAGRAPH.
           MOVE   4500                     TO WSLG-SQL-NBR.
           MOVE 'UPDATE'                   TO WSLG-DB2-STATEMENT.
           MOVE 'APD_APRVL_AUD_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               UPDATE  APD_APRVL_AUD_V
                  SET  USER_CD            = :APD-USER-CD
                      ,APRVL_CD           = :APD-APRVL-CD
                      ,ALT_APRVL_CD       = :APD-ALT-APRVL-CD
                      ,APRVL_DT           =  CURRENT_DATE
                      ,APRVL_TIME         =  CURRENT_TIME
                WHERE  FK_APH_FSCL_YR     = :APD-FK-APH-FSCL-YR
                  AND  FK_APH_SEQ_NBR     = :APD-FK-APH-SEQ-NBR
                  AND  FK_APH_DOC_NBR     = :APD-FK-APH-DOC-NBR
                  AND  FK_APH_CHG_SEQ_NBR = :APD-FK-APH-CHG-SEQ-NBR
                  AND  LEVEL_SEQ_NBR      = :APD-LEVEL-SEQ-NBR
           END-EXEC.

           MOVE SQLCODE                    TO APD-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                       TO WS-UPDATE-COUNT
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S4600-UPDATE-APH-NEXT-LVL.
      ******************************************************************
      *    UPDATE APH_APRVL_HDR_V FOREIGN KEY TO THE NEXT APL_APRVL_LVL.
      ******************************************************************

           MOVE 'S4600-UPDATE-APH-NEXT-LVL'
                                           TO WSLG-PARAGRAPH.
           MOVE   4600                     TO WSLG-SQL-NBR.
           MOVE 'UPDATE'                   TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               UPDATE APH_APRVL_HDR_V
                  SET APRVL_ID           = :APH-APRVL-ID
                     ,FK_APL_SEQ_NBR     = :APH-FK-APL-SEQ-NBR
                     ,FK_APL_USER_ID     = :APH-FK-APL-USER-ID
                     ,FK_APL_APV_TPLT_CD = :APH-FK-APL-APV-TPLT-CD
                     ,FK_APL_LVL_SEQ_NBR = :APH-FK-APL-LVL-SEQ-NBR
                     ,APL_SET_TS         =  CURRENT_TIMESTAMP
                     ,APL_SETF           = 'Y'
                WHERE SEQ_NBR            = :APH-SEQ-NBR
                  AND FSCL_YR            = :APH-FSCL-YR
                  AND DOC_NBR            = :APH-DOC-NBR
                  AND CHNG_SEQ_NBR       = :APH-CHNG-SEQ-NBR
           END-EXEC.

           MOVE SQLCODE                    TO APH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD +1                      TO WS-UPDATE-COUNT
             WHEN -911
             WHEN -913
               MOVE 'S46001'               TO UPDAPRVL-RETURN-MSG-NUM
               MOVE 'SEQ-NBR/FSCL-YR/DOC-NBR/CHGSEQ'
                                           TO UPDAPRVL-RETURN-MSG-FIELD
               MOVE 'SYSTEM DEADLOCK - PLEASE TRY AGAIN'
                                           TO UPDAPRVL-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S4700-UPDATE-APH-FINAL.
      ******************************************************************
      *    UPDATE APH_APRVL_HDR_V FOR FINAL APPROVAL.
      ******************************************************************

           MOVE 'S4700-UPDATE-APH-FINAL'   TO WSLG-PARAGRAPH.
           MOVE   4700                     TO WSLG-SQL-NBR.
           MOVE 'UPDATE'                   TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               UPDATE APH_APRVL_HDR_V
                  SET APRVL_IND          = :APH-APRVL-IND
                     ,IDX_APRVL_SETF     = :APH-IDX-APRVL-SETF
                     ,IDX_UNAPRVD_SETF   = :APH-IDX-UNAPRVD-SETF
                     ,APL_SETF           = :APH-APL-SETF
                     ,DOC_AMT            = :APH-DOC-AMT
                WHERE SEQ_NBR            = :APH-SEQ-NBR
                  AND FSCL_YR            = :APH-FSCL-YR
                  AND DOC_NBR            = :APH-DOC-NBR
                  AND CHNG_SEQ_NBR       = :APH-CHNG-SEQ-NBR
           END-EXEC.

           MOVE SQLCODE                    TO APH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               ADD +1                      TO WS-UPDATE-COUNT
             WHEN -911
             WHEN -913
               MOVE 'S47001'               TO UPDAPRVL-RETURN-MSG-NUM
               MOVE 'SEQ-NBR/FSCL-YR/DOC-NBR/CHGSEQ'
                                           TO UPDAPRVL-RETURN-MSG-FIELD
               MOVE 'SYSTEM DEADLOCK - PLEASE TRY AGAIN'
                                           TO UPDAPRVL-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       Z9998-LOG-ERROR-VIA-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W TO LOG 2 ERROR MESSAGES TO THE WSLG
      *    TRANSIENT DATA QUEUE AND, IF REQUESTED, TO THE Z/OS CONSOLE.
      *    THE FIRST MESSAGE (SEQ# 0) CONTAINS DESCRIPTIVE ERROR TEXT.
      *    THE SECOND MESSAGE (SEQ# 1) CONTAINS DIAGNOSTIC INFORMATION.
      *    ROLLBACK ANY DB2 UPDATES THAT MAY HAVE OCCURRED THIS TASK.
      ******************************************************************

           MOVE '0'                    TO WSLG-MSG-SEQ-NUM.
           MOVE 'WUT003U'              TO WSLG-PROGRAM-NAME.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           MOVE '1'                    TO WSLG-MSG-SEQ-NUM.
           MOVE EIBTASKN               TO WSLG-CICS-TASK-NUMBER.
           MOVE WSLG-DIAGNOSTIC-AREA   TO WSLG-MSG-TEXT.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           IF WS-UPDATE-COUNT > 0
               MOVE 0                  TO WS-UPDATE-COUNT
               EXEC CICS SYNCPOINT ROLLBACK
               END-EXEC
           END-IF.


       Z9998-LINK-TO-PROGRAM-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W.  IF THE LINK FAILS,
      *    ABEND THE TASK WITH A 'WCIX' DUMP CODE.
      ******************************************************************

           EXEC CICS LINK
                PROGRAM (WSLG-PROGRAM-UIS000W)
                COMMAREA(WSLG-USING-LIST)
                RESP    (WSLG-CICS-RESP)
                RESP2   (WSLG-CICS-RESP2)
           END-EXEC.

           IF WSLG-CICS-RESP NOT = DFHRESP(NORMAL)
               EXEC CICS ABEND
                    ABCODE('WCIX')
               END-EXEC
           END-IF.
      /
       Z9999-RETURN-DATA-ERROR.
      ******************************************************************
      *    RETURN THE DATA ERROR MESSAGE TO THE CALLING PROGRAM.
      *    ROLLBACK ANY DB2 UPDATES THAT MAY HAVE OCCURRED THIS TASK.
      ******************************************************************

           MOVE 'WFUT999E'             TO UPDAPRVL-VERS-RESP-CD.
           MOVE 'E'                    TO UPDAPRVL-RETURN-STATUS-CODE.
           MOVE  1                     TO UPDAPRVL-RETURN-MSG-COUNT.

           IF WS-UPDATE-COUNT > 0
               MOVE 0                  TO WS-UPDATE-COUNT
               EXEC CICS SYNCPOINT ROLLBACK
               END-EXEC
           END-IF.

           GO TO Z9999-RETURN-TO-CALLING-PGM.
      /
       Z9999-RETURN-DB2-ABEND.
      ******************************************************************
      *    LOG THE FATAL DB2 ERROR VIA PROGRAM UIS000W
      *    AND ABEND THE TASK WITH A 'WSQL' DUMP CODE.
      ******************************************************************

           MOVE WSLG-FATAL-DB2-MSG-ID  TO WSLG-MSG-ID.
           MOVE WSLG-DB2-STATEMENT     TO WSLG-FATAL-DB2-STATEMENT.
           MOVE WSLG-DB2-TABLE         TO WSLG-FATAL-DB2-TABLE.
           MOVE WSLG-FATAL-DB2-MSG-TEXT
                                       TO WSLG-MSG-TEXT.
           MOVE 'Y'                    TO WSLG-CONSOLE-MSG-IND.
           MOVE SPACES                 TO WSLG-DIAGNOSTIC-INFO.
           MOVE SQLCODE                TO WSLG-SQL-CODE.
           STRING 'SQL#: '                WSLG-SQL-NBR
                  '  SQLCODE: '           WSLG-SQL-CODE
                   DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                     INTO WSLG-DIAGNOSTIC-INFO.
           PERFORM Z9998-LOG-ERROR-VIA-UIS000W.

           EXEC CICS ABEND
                ABCODE('WSQL')
           END-EXEC.
      /
       Z9999-RETURN-SUCCESSFUL.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM WITH SUCCESSFUL STATUS.
      ******************************************************************

           MOVE 'WFUT000I'             TO UPDAPRVL-VERS-RESP-CD.
           MOVE 'S'                    TO UPDAPRVL-RETURN-STATUS-CODE.
           MOVE  1                     TO UPDAPRVL-RETURN-MSG-COUNT.

           GO TO Z9999-RETURN-TO-CALLING-PGM.


       Z9999-RETURN-TO-CALLING-PGM.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM.
      ******************************************************************

           GOBACK.
