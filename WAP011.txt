       IDENTIFICATION DIVISION.
       PROGRAM-ID.    WAP011.
      ******************************************************************
      *    TITLE ........... DELETE SCIQUEST INVOICE
      *                     (DELINVCE)
      *    LANGUAGE ........ COBOL Z/OS, DB2, CICS
      *    AUTHOR .......... H. VANDERWEIT
      *    DATE-WRITTEN .... AUGUST, 2007
      *
      *                      DESCRIPTION
      *                      -----------
      *    THIS WEB SERVICE RECEIVES EITHER AN INVOICE NUMBER OR A
      *    BATCH-ID FROM SCIQUEST AND DELETES THE ASSOCIATED INVOICE(S)
      *    FROM THE IFIS TABLES LISTED BELOW.  FOR BATCH-ID REQUESTS,
      *    THE ASSOCIATED INVOICES ARE IDENTIFIED ON THE APH_APRVL_HDR
      *    TABLE.
      *
      *    DB2 TABLE(S)      HOW ACCESSED
      *    ----------------  -----------------------------------------
      *    APH_APRVL_HDR_V   FETCH, DELETE (SEQ_NBR = 0003)
      *    IVH_INV_HDR_V     SELECT,DELETE
      *    IVD_INV_DTL_V     DELETE
      *    IVA_INV_ACCT_V    DELETE
      *    TXH_TXT_HDR_V     DELETE (SEQ_NBR = 0003)
      *    TXD_TXT_DTL_V     DELETE (SEQ_NBR = 0003)
      *    APD_APRVL_AUD_V   DELETE (SEQ_NBR = 0003)
DEVHV *    APR_APRVD_V       COMMENTED OUT DELETE (SEQ_NBR = 0003)
      *
      ******************************************************************
      *                      MAINTENANCE HISTORY                       *
      ******************************************************************
001MLJ*    MODIFIED BY ..... MARILYN JEWETT
      *    DATE MODIFIED ... 06/11/09
      *    MODIFICATION .... MY ENTERTAINMENT CHANGES
      ******************************************************************
      /
       ENVIRONMENT DIVISION.

       DATA DIVISION.

       WORKING-STORAGE SECTION.
       01  FILLER                      PIC X(36)       VALUE
           'WAP011 WORKING-STORAGE BEGINS HERE '.

       01  WS-MISCELLANEOUS.
           05  WS-UPDATE-COUNT         PIC S9(4) COMP  VALUE ZEROES.
           05  WS-DELETING-BATCH-SW    PIC X(01)       VALUE SPACES.

           05  WS-CICS-SYSID.
               10  FILLER              PIC X(03)       VALUE SPACES.
               10  WS-CICS-REGION      PIC X(01)       VALUE SPACES.

           05  WS-LIKE-DOC-NBR         PIC X(09) VALUE '00000000%'.
           05  WS-LIKE-DOC-NBR-GROUP   REDEFINES WS-LIKE-DOC-NBR.
               10  WS-DOC-NBR          PIC X(08).
               10  FILLER              PIC X(01).

       01  WSLG-USING-LIST.
           05  WSLG-CONSOLE-MSG-IND    PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-ID             PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-DATE           PIC X(10)       VALUE SPACES.
           05  WSLG-MSG-TIME           PIC X(08)       VALUE SPACES.
           05  WSLG-PROGRAM-NAME       PIC X(08)       VALUE SPACES.
           05  WSLG-MSG-SEQ-NUM        PIC X(01)       VALUE SPACES.
           05  WSLG-MSG-TEXT           PIC X(80)       VALUE SPACES.

       01  WSLG-DIAGNOSTIC-AREA.
           05  FILLER                  PIC X(05)       VALUE 'PARA:'.
           05  WSLG-PARAGRAPH          PIC X(30)       VALUE SPACES.
           05  FILLER                  PIC X(05)       VALUE 'TASK:'.
           05  WSLG-CICS-TASK-NUMBER   PIC X(08)       VALUE SPACES.
           05  FILLER                  PIC X(02)       VALUE SPACES.
           05  WSLG-DIAGNOSTIC-INFO    PIC X(30)       VALUE SPACES.

       01  WSLG-MISC-AREA.
           05  WSLG-PROGRAM-UIS000W    PIC X(08)       VALUE 'UIS000W '.
           05  WSLG-CICS-RESP          PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-CICS-RESP2         PIC S9(8) COMP  VALUE ZEROES.
           05  WSLG-EIBCALEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-CHANLLEN-NUMEDIT   PIC ZZZZ9       VALUE ZEROES.
           05  WSLG-DB2-STATEMENT      PIC X(08)       VALUE SPACES.
           05  WSLG-DB2-TABLE          PIC X(16)       VALUE SPACES.
           05  WSLG-SQL-NBR            PIC 9(04)       VALUE ZEROES.
           05  WSLG-SQL-CODE           PIC +++++9      VALUE ZEROES.

       01  WSLG-MESSAGE-AREA.
           05  WSLG-SUCCESSFUL-MSG-ID  PIC X(08)       VALUE 'WFAP000I'.
           05  WSLG-SUCCESSFUL-MSG-TEXT
                                       PIC X(40)       VALUE
               'The request was successfully completed. '.

           05  WSLG-CHNL-ID-MSG-ID     PIC X(08)       VALUE 'WFAP001E'.
           05  WSLG-CHNL-ID-MSG-TEXT   PIC X(80)       VALUE
               'The received channel ID value is unrecognized.'.

           05  WSLG-CHNL-VERS-MSG-ID   PIC X(08)       VALUE 'WFAP002E'.
           05  WSLG-CHNL-VERS-MSG-TEXT PIC X(80)       VALUE
               'The received channel version value is unrecognized.'.

           05  WSLG-CHNL-LGTH-MSG-ID   PIC X(08)       VALUE 'WFAP003E'.
           05  WSLG-CHNL-LGTH-MSG-TEXT PIC X(80)       VALUE
               'The received channel length is not equal to the defined
      -        'channel version length. '.

           05  WSLG-FATAL-DB2-MSG-ID   PIC X(08)       VALUE 'WFAP004S'.
           05  WSLG-FATAL-DB2-MSG-TEXT.
               10  FILLER              PIC X(49)       VALUE
                   'DB2 returned a severe SQL error on an attempt to '.
               10  WSLG-FATAL-DB2-STATEMENT
                                       PIC X(08)       VALUE SPACES.
               10  FILLER              PIC X(07)       VALUE ' table '.
               10  WSLG-FATAL-DB2-TABLE
                                       PIC X(16)       VALUE SPACES.
      /
      ******************************************************************
      *01  DELINVCE-PARM-AREA.
      ******************************************************************
           COPY WAP011.
      /
      ******************************************************************
      *    SQL COMMAREA
      ******************************************************************
           EXEC SQL INCLUDE SQLCA
           END-EXEC.
      /
      ******************************************************************
      *    APH_APRVL_HDR_V
      ******************************************************************
       01  APH-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'APH*'.
           05  APH-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVAPH
           END-EXEC.
      /
      ******************************************************************
      *    IVH_INV_HDR_V
      ******************************************************************
       01  IVH-SQLAREA.
           05  FILLER                  PIC X(04)       VALUE 'IVH*'.
           05  IVH-SQLCODE             PIC S9(9)       COMP-4
                                                       VALUE +0.
           EXEC SQL INCLUDE HVIVH
           END-EXEC.
      /
       LINKAGE SECTION.
      /
       01  DFHCOMMAREA.
           05  COMM-CHANNEL-HEADER.
               10  COMM-CHANNEL-ID     PIC X(08).
                   88  COMM-CHANNEL-ID-OK              VALUE 'DELINVCE'.
               10  COMM-VERS-RESP-CD   PIC X(08).
                   88  COMM-VERSION-01-00-00           VALUE '01.00.00'.
           05  COMM-DELINVCE-INPUT-PARMS
                                       PIC X(500).

       PROCEDURE DIVISION.

      /
       A0000-MAINLINE-ROUTINE.
      ******************************************************************
      *    PERFORM ROUTINE TO EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      *    IF AN INVOICE NUMBER HAS BEEN PASSED, PERFORM THE ROUTINE
      *    TO DELETE IT.
      *    IF AN INVOICE NUMBER HAS NOT BEEN PASSED BUT A SCIQUEST
      *    BATCH-ID HAS, PERFORM THE ROUTINE TO RETRIEVE AND DELETE
      *    EACH ASSOCIATED INVOICE.
      *    RETURN CONTROL TO THE SCIQUEST/WEB APPLICATION.
      ******************************************************************

           MOVE 'A0000-MAINLINE-ROUTINE'
                                       TO WSLG-PARAGRAPH.
           PERFORM A0500-EDIT-COMMAREA-VERSION.

           MOVE DFHCOMMAREA            TO DELINVCE-PARM-AREA.
           INITIALIZE DELINVCE-RETURN-STATUS-PARMS.

           EXEC CICS ASSIGN
                     SYSID (WS-CICS-SYSID)
           END-EXEC.

           IF DELINVCE-DOC-NBR > SPACES
               MOVE 'N'                TO WS-DELETING-BATCH-SW
               MOVE DELINVCE-DOC-NBR   TO IVH-DOC-NBR
               PERFORM S0500-SELECT-IVH

               PERFORM A5000-DELETE-INVOICE-TABLES
           ELSE
               IF DELINVCE-SCIQ-BATCH-ID > SPACES
                   MOVE 'Y'            TO WS-DELETING-BATCH-SW
                   PERFORM A1000-DELETE-BATCH-ROUTINE
               ELSE
                   MOVE 'A00001'       TO DELINVCE-RETURN-MSG-NUM
                   MOVE 'DOC-NBR'      TO DELINVCE-RETURN-MSG-FIELD
                   MOVE 'Doc-Nbr or batch-id must be entered'
                                       TO DELINVCE-RETURN-MSG-TEXT
                   GO TO Z9999-RETURN-DATA-ERROR
               END-IF
           END-IF.

           GO TO Z9999-RETURN-SUCCESSFUL.
      /
       A0500-EDIT-COMMAREA-VERSION.
      ******************************************************************
      *    EDIT THE COMMAREA ID, VERSION, AND LENGTH.
      ******************************************************************

           MOVE 'A0500-EDIT-COMMAREA-VERSION'
                                       TO WSLG-PARAGRAPH.
           INSPECT COMM-CHANNEL-ID
                   CONVERTING 'abcdefghijklmnopqrstuvwxyz'
                           TO 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.

           INITIALIZE                     DELINVCE-PARM-AREA.
           MOVE COMM-CHANNEL-HEADER    TO DELINVCE-CHANNEL-HEADER.

           IF COMM-CHANNEL-ID-OK
               EVALUATE TRUE
               WHEN COMM-VERSION-01-00-00

                   IF EIBCALEN = LENGTH OF DELINVCE-PARM-AREA
                       NEXT SENTENCE
                   ELSE
                       SET DELINVCE-RESPONSE-ERROR
                                       TO TRUE
                       MOVE 'E'        TO DELINVCE-RETURN-STATUS-CODE   E
                       MOVE  1         TO DELINVCE-RETURN-MSG-COUNT
                       MOVE 'A05001'   TO DELINVCE-RETURN-MSG-NUM
                       MOVE 'Channel-Id'
                                       TO DELINVCE-RETURN-MSG-FIELD
                       MOVE 'The channel length is invalid           '
                                       TO DELINVCE-RETURN-MSG-TEXT
                       MOVE WSLG-CHNL-LGTH-MSG-ID
                                       TO WSLG-MSG-ID
                       MOVE WSLG-CHNL-LGTH-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                       MOVE 'N'        TO WSLG-CONSOLE-MSG-IND
                       MOVE SPACES     TO WSLG-DIAGNOSTIC-INFO
                       MOVE EIBCALEN   TO WSLG-EIBCALEN-NUMEDIT
                       MOVE LENGTH OF DELINVCE-PARM-AREA
                                       TO WSLG-CHANLLEN-NUMEDIT
                       STRING 'EIBCALEN ' WSLG-EIBCALEN-NUMEDIT
                              ' versus '  WSLG-CHANLLEN-NUMEDIT
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                       PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                       GO TO Z9999-RETURN-TO-CALLING-PGM
                   END-IF

               WHEN OTHER
                   SET DELINVCE-RESPONSE-ERROR
                                       TO TRUE
                   MOVE 'E'            TO DELINVCE-RETURN-STATUS-CODE
                   MOVE  1             TO DELINVCE-RETURN-MSG-COUNT
                   MOVE 'A05002'       TO DELINVCE-RETURN-MSG-NUM
                   MOVE 'Channel-Id'   TO DELINVCE-RETURN-MSG-FIELD
                   MOVE 'The channel version is invalid          '
                                       TO DELINVCE-RETURN-MSG-TEXT
                   MOVE WSLG-CHNL-VERS-MSG-ID
                                       TO WSLG-MSG-ID
                   MOVE WSLG-CHNL-VERS-MSG-TEXT
                                       TO WSLG-MSG-TEXT
                   MOVE 'N'            TO WSLG-CONSOLE-MSG-IND
                   MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
                   STRING 'Received version: ' COMM-VERS-RESP-CD
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
                   PERFORM Z9998-LOG-ERROR-VIA-UIS000W
                   GO TO Z9999-RETURN-TO-CALLING-PGM
               END-EVALUATE
           ELSE
               SET DELINVCE-RESPONSE-ERROR
                                       TO TRUE
               MOVE 'E'                TO DELINVCE-RETURN-STATUS-CODE
               MOVE  1                 TO DELINVCE-RETURN-MSG-COUNT
               MOVE 'A05003'           TO DELINVCE-RETURN-MSG-NUM
               MOVE 'Channel-Id'       TO DELINVCE-RETURN-MSG-FIELD
               MOVE 'The channel id value is invalid         '
                                       TO DELINVCE-RETURN-MSG-TEXT
               MOVE WSLG-CHNL-ID-MSG-ID
                                       TO WSLG-MSG-ID
               MOVE WSLG-CHNL-ID-MSG-TEXT
                                       TO WSLG-MSG-TEXT
               MOVE 'N'                TO WSLG-CONSOLE-MSG-IND
               MOVE SPACES             TO WSLG-DIAGNOSTIC-INFO
               STRING 'Received Channel Id: ' COMM-CHANNEL-ID
                       DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                         INTO WSLG-DIAGNOSTIC-INFO
               PERFORM Z9998-LOG-ERROR-VIA-UIS000W
               GO TO Z9999-RETURN-TO-CALLING-PGM
           END-IF.
      /
       A1000-DELETE-BATCH-ROUTINE.
      ******************************************************************
      *    PROCESS THE APH-CURSOR TO DELETE EVERY INVOICE ASSOCIATED
      *    TO THE PASSED SCIQUEST BATCH-ID.
      ******************************************************************

           MOVE 'A1000-DELETE-BATCH-ROUTINE'
                                       TO WSLG-PARAGRAPH.

           MOVE DELINVCE-SCIQ-BATCH-ID TO APH-SCIQ-BATCH-ID.

           PERFORM S1000-OPEN-APH-CURSOR.

           PERFORM S1100-FETCH-APH-CURSOR.

           IF APH-SQLCODE = +100
               PERFORM S1200-CLOSE-APH-CURSOR
               MOVE 'A10001'           TO DELINVCE-RETURN-MSG-NUM
               MOVE 'SCIQ-BATCH-ID'    TO DELINVCE-RETURN-MSG-FIELD
               MOVE 'Batch id does not exist - cannot delete '
                                       TO DELINVCE-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           PERFORM WITH TEST AFTER
                   UNTIL APH-SQLCODE = +100

               MOVE APH-DOC-NBR        TO IVH-DOC-NBR
               MOVE APH-APRVL-IND      TO IVH-APRVL-IND
               PERFORM A5000-DELETE-INVOICE-TABLES

               PERFORM S1100-FETCH-APH-CURSOR
           END-PERFORM.

           PERFORM S1200-CLOSE-APH-CURSOR.
      /
       A5000-DELETE-INVOICE-TABLES.
      ******************************************************************
      *    DO NOT ALLOW DELETION OF A NON-SCIQUEST INVOICE NUMBER.
      *    DO NOT ALLOW AN APPROVED INVOICE TO BE DELETED IN PRODUCTION.
      *    OTHERWISE, DELETE ALL INVOICE-ASSOCIATED TABLES FOR THE
      *    INVOICE NUMBER.
      ******************************************************************

           MOVE 'A5000-DELETE-INVOICE-TABLES'
                                       TO WSLG-PARAGRAPH.

001MLJ     IF IVH-DOC-NBR(1:1) = '8' OR 'E'
               NEXT SENTENCE
           ELSE
               IF WS-DELETING-BATCH-SW = 'Y'
                  MOVE 'A50001'        TO DELINVCE-RETURN-MSG-NUM
                  MOVE 'SCIQ-BATCH-ID' TO DELINVCE-RETURN-MSG-FIELD
                  MOVE 'Batch id contains non-Sciquest invoice  '
                                       TO DELINVCE-RETURN-MSG-TEXT
               ELSE
                  MOVE 'A50002'        TO DELINVCE-RETURN-MSG-NUM
                  MOVE 'DOC-NBR'       TO DELINVCE-RETURN-MSG-FIELD
                  MOVE 'Invoice number must begin with 8'
                                       TO DELINVCE-RETURN-MSG-TEXT
               END-IF
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           IF  IVH-APRVL-IND  = 'Y'
DEVHV******AND WS-CICS-REGION = 'P'
               IF WS-DELETING-BATCH-SW = 'Y'
                  MOVE 'A50003'        TO DELINVCE-RETURN-MSG-NUM
                  MOVE 'SCIQ-BATCH-ID' TO DELINVCE-RETURN-MSG-FIELD
                  MOVE 'Batch id contains approved invoice'
                                       TO DELINVCE-RETURN-MSG-TEXT
               ELSE
                  MOVE 'A50004'        TO DELINVCE-RETURN-MSG-NUM
                  MOVE 'DOC-NBR'       TO DELINVCE-RETURN-MSG-FIELD
                  MOVE 'Invoice has been approved- cannot delete'
                                       TO DELINVCE-RETURN-MSG-TEXT
               END-IF
               GO TO Z9999-RETURN-DATA-ERROR
           END-IF.

           MOVE IVH-DOC-NBR            TO WS-DOC-NBR.

           PERFORM S2100-DELETE-IVH.
           PERFORM S2200-DELETE-IVD.
           PERFORM S2300-DELETE-IVA.
           PERFORM S2400-DELETE-TXH.
           PERFORM S2500-DELETE-TXD.
           PERFORM S2600-DELETE-APH.
           PERFORM S2700-DELETE-APD.
DEVHV******PERFORM S2800-DELETE-APR.
      /
       S0500-SELECT-IVH.
      ******************************************************************
      *    SELECT IVH_INV_HDR_V
      ******************************************************************

           MOVE 'S0500-SELECT-IVH'     TO WSLG-PARAGRAPH.
           MOVE   0500                 TO WSLG-SQL-NBR.
           MOVE 'SELECT'               TO WSLG-DB2-STATEMENT.
           MOVE 'IVH_INV_HDR_V'        TO WSLG-DB2-TABLE.

           EXEC SQL
               SELECT IVH.APRVL_IND
                INTO :IVH-APRVL-IND
                 FROM IVH_INV_HDR_V   IVH
                WHERE IVH.DOC_NBR  = :IVH-DOC-NBR
           END-EXEC.

           MOVE SQLCODE                TO IVH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN +100
               MOVE 'S05001'           TO DELINVCE-RETURN-MSG-NUM
               MOVE 'DOC-NBR'          TO DELINVCE-RETURN-MSG-FIELD
               MOVE 'Invoice does not exist - cannot delete   '
                                       TO DELINVCE-RETURN-MSG-TEXT
               GO TO Z9999-RETURN-DATA-ERROR
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S1000-OPEN-APH-CURSOR.
      ******************************************************************
      *    OPEN APH_APRVL_HDR_V CURSOR
      ******************************************************************

           MOVE 'S1000-OPEN-APH-CURSOR'
                                       TO WSLG-PARAGRAPH.
           MOVE   1000                 TO WSLG-SQL-NBR.
           MOVE 'OPEN'                 TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'      TO WSLG-DB2-TABLE.

           EXEC SQL
             DECLARE APH-CURSOR CURSOR FOR
               SELECT APH.DOC_NBR
                     ,APH.APRVL_IND
                 FROM APH_APRVL_HDR_V      APH
                WHERE APH.SCIQ_BATCH_ID = :APH-SCIQ-BATCH-ID
                  AND APH.SEQ_NBR       =  0003
           END-EXEC.

           EXEC SQL
               OPEN APH-CURSOR
           END-EXEC.

           MOVE SQLCODE                TO APH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S1100-FETCH-APH-CURSOR.
      ******************************************************************
      *    FETCH APH_APRVL_HDR_V CURSOR
      ******************************************************************

           MOVE 'S1100-FETCH-APH-CURSOR'
                                       TO WSLG-PARAGRAPH.
           MOVE   1100                 TO WSLG-SQL-NBR.
           MOVE 'FETCH'                TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'      TO WSLG-DB2-TABLE.

           EXEC SQL
               FETCH APH-CURSOR
                INTO  :APH-DOC-NBR
                     ,:APH-APRVL-IND
           END-EXEC.

           MOVE SQLCODE                TO APH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S1200-CLOSE-APH-CURSOR.
      ******************************************************************
      *    CLOSE APH_APRVL_HDR_V CURSOR
      ******************************************************************

           MOVE 'S1200-CLOSE-APH-CURSOR'
                                       TO WSLG-PARAGRAPH.
           MOVE   1200                 TO WSLG-SQL-NBR.
           MOVE 'CLOSE'                TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'      TO WSLG-DB2-TABLE.

           EXEC SQL
               CLOSE APH-CURSOR
           END-EXEC.

           MOVE SQLCODE                TO APH-SQLCODE.

           EVALUATE SQLCODE
             WHEN +0
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2100-DELETE-IVH.
      ******************************************************************
      *    DELETE IVH_INV_HDR_V.
      ******************************************************************

           MOVE 'S2100-DELETE-IVH'     TO WSLG-PARAGRAPH.
           MOVE   2100                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'IVH_INV_HDR_V'        TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE
                 FROM IVH_INV_HDR_V
                WHERE DOC_NBR          = :WS-DOC-NBR
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO WS-UPDATE-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2200-DELETE-IVD.
      ******************************************************************
      *    DELETE IVD_INV_DTL_V.
      ******************************************************************

           MOVE 'S2200-DELETE-IVD'     TO WSLG-PARAGRAPH.
           MOVE   2200                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'IVD_INV_DTL_V'        TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE
                 FROM IVD_INV_DTL_V
                WHERE FK_IVH_DOC_NBR   = :WS-DOC-NBR
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO WS-UPDATE-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2300-DELETE-IVA.
      ******************************************************************
      *    DELETE IVA_INV_ACCT_V.
      ******************************************************************

           MOVE 'S2300-DELETE-IVA'     TO WSLG-PARAGRAPH.
           MOVE   2300                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'IVA_INV_ACCT_V'       TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE
                 FROM IVA_INV_ACCT_V
                WHERE FK_IVD_DOC_NBR   = :WS-DOC-NBR
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO WS-UPDATE-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2400-DELETE-TXH.
      ******************************************************************
      *    DELETE TXH_TXT_HDR_V.
      ******************************************************************

           MOVE 'S2400-DELETE-TXH'     TO WSLG-PARAGRAPH.
           MOVE   2400                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'TXH_TXT_HDR_V'        TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE
                 FROM TXH_TXT_HDR_V
                WHERE DOC_TYP_SEQ_NBR  =  0003
                  AND TEXT_ENTY_CD  LIKE :WS-LIKE-DOC-NBR
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO WS-UPDATE-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2500-DELETE-TXD.
      ******************************************************************
      *    DELETE TXD_TXT_DTL_V.
      ******************************************************************

           MOVE 'S2500-DELETE-TXD'     TO WSLG-PARAGRAPH.
           MOVE   2500                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'TXD_TXT_DTL_V'        TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE
                 FROM TXD_TXT_DTL_V
                WHERE FK_TXH_DOC_TYP_SEQ    =  0003
                  AND FK_TXH_TXT_ENTYCD  LIKE :WS-LIKE-DOC-NBR
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO WS-UPDATE-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2600-DELETE-APH.
      ******************************************************************
      *    DELETE APH_APRVL_HDR_V.
      ******************************************************************

           MOVE 'S2600-DELETE-APH'     TO WSLG-PARAGRAPH.
           MOVE   2600                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'APH_APRVL_HDR_V'      TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE
                 FROM APH_APRVL_HDR_V
                WHERE SEQ_NBR          =  0003
                  AND FSCL_YR          = '  '
                  AND DOC_NBR          = :WS-DOC-NBR
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO WS-UPDATE-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2700-DELETE-APD.
      ******************************************************************
      *    DELETE APD_APRVL_AUD_V.
      ******************************************************************

           MOVE 'S2700-DELETE-APD'     TO WSLG-PARAGRAPH.
           MOVE   2700                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'APD_APRVL_AUD_V'      TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE
                 FROM APD_APRVL_AUD_V
                WHERE FK_APH_SEQ_NBR   =  0003
                  AND FK_APH_FSCL_YR   = '  '
                  AND FK_APH_DOC_NBR   = :WS-DOC-NBR
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO WS-UPDATE-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       S2800-DELETE-APR.
      ******************************************************************
      *    DELETE APR_APRVD_V.
      ******************************************************************

           MOVE 'S2800-DELETE-APR'     TO WSLG-PARAGRAPH.
           MOVE   2800                 TO WSLG-SQL-NBR.
           MOVE 'DELETE'               TO WSLG-DB2-STATEMENT.
           MOVE 'APR_APRVD_V'          TO WSLG-DB2-TABLE.

           EXEC SQL
               DELETE
                 FROM APR_APRVD_V
                WHERE SEQ_NBR          =  0003
                  AND DOC_NBR          = :WS-DOC-NBR
           END-EXEC.

           EVALUATE SQLCODE
             WHEN +0
               ADD 1                   TO WS-UPDATE-COUNT
             WHEN +100
               CONTINUE
             WHEN OTHER
               GO TO Z9999-RETURN-DB2-ABEND
           END-EVALUATE.
      /
       Z9998-LOG-ERROR-VIA-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W TO LOG 2 ERROR MESSAGES TO THE WSLG
      *    TRANSIENT DATA QUEUE AND, IF REQUESTED, TO THE Z/OS CONSOLE.
      *    THE FIRST MESSAGE (SEQ# 0) CONTAINS DESCRIPTIVE ERROR TEXT.
      *    THE SECOND MESSAGE (SEQ# 1) CONTAINS DIAGNOSTIC INFORMATION.
      *    ROLLBACK ANY DB2 UPDATES THAT MAY HAVE OCCURRED THIS TASK.
      ******************************************************************

           MOVE '0'                    TO WSLG-MSG-SEQ-NUM.
           MOVE 'WAP011'               TO WSLG-PROGRAM-NAME.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           MOVE '1'                    TO WSLG-MSG-SEQ-NUM.
           MOVE EIBTASKN               TO WSLG-CICS-TASK-NUMBER.
           MOVE WSLG-DIAGNOSTIC-AREA   TO WSLG-MSG-TEXT.
           PERFORM Z9998-LINK-TO-PROGRAM-UIS000W.

           IF WS-UPDATE-COUNT > 0
               MOVE 0                  TO WS-UPDATE-COUNT
               EXEC CICS SYNCPOINT ROLLBACK
               END-EXEC
           END-IF.


       Z9998-LINK-TO-PROGRAM-UIS000W.
      ******************************************************************
      *    LINK TO PROGRAM UIS000W.  IF THE LINK FAILS,
      *    ABEND THE TASK WITH A 'WCIX' DUMP CODE.
      ******************************************************************

           EXEC CICS LINK
                PROGRAM (WSLG-PROGRAM-UIS000W)
                COMMAREA(WSLG-USING-LIST)
                RESP    (WSLG-CICS-RESP)
                RESP2   (WSLG-CICS-RESP2)
           END-EXEC.

           IF WSLG-CICS-RESP NOT = DFHRESP(NORMAL)
               EXEC CICS ABEND
                    ABCODE('WCIX')
               END-EXEC
           END-IF.
      /
       Z9999-RETURN-DATA-ERROR.
      ******************************************************************
      *    RETURN THE DATA ERROR MESSAGE TO THE CALLING PROGRAM.
      *    ROLLBACK ANY DB2 UPDATES THAT MAY HAVE OCCURRED THIS TASK.
      ******************************************************************

           SET DELINVCE-RESPONSE-ERROR TO TRUE.
           MOVE 'E'                    TO DELINVCE-RETURN-STATUS-CODE.
           MOVE  1                     TO DELINVCE-RETURN-MSG-COUNT.

           IF WS-UPDATE-COUNT > 0
               MOVE 0                  TO WS-UPDATE-COUNT
               EXEC CICS SYNCPOINT ROLLBACK
               END-EXEC
           END-IF.

           GO TO Z9999-RETURN-TO-CALLING-PGM.
      /
       Z9999-RETURN-DB2-ABEND.
      ******************************************************************
      *    LOG THE FATAL DB2 ERROR VIA PROGRAM UIS000W
      *    AND ABEND THE TASK WITH A 'WSQL' DUMP CODE.
      ******************************************************************

           MOVE WSLG-FATAL-DB2-MSG-ID  TO WSLG-MSG-ID.
           MOVE WSLG-DB2-STATEMENT     TO WSLG-FATAL-DB2-STATEMENT.
           MOVE WSLG-DB2-TABLE         TO WSLG-FATAL-DB2-TABLE.
           MOVE WSLG-FATAL-DB2-MSG-TEXT
                                       TO WSLG-MSG-TEXT.
           MOVE 'Y'                    TO WSLG-CONSOLE-MSG-IND.
           MOVE SPACES                 TO WSLG-DIAGNOSTIC-INFO.
           MOVE SQLCODE                TO WSLG-SQL-CODE.
           STRING 'SQL#: '                WSLG-SQL-NBR
                  '  SQLCODE: '           WSLG-SQL-CODE
                   DELIMITED BY LENGTH OF WSLG-DIAGNOSTIC-INFO
                                     INTO WSLG-DIAGNOSTIC-INFO.
           PERFORM Z9998-LOG-ERROR-VIA-UIS000W.

           EXEC CICS ABEND
                ABCODE('WSQL')
           END-EXEC.
      /
       Z9999-RETURN-SUCCESSFUL.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM WITH SUCCESSFUL STATUS.
      ******************************************************************

           SET DELINVCE-RESPONSE-SUCCESSFUL
                                       TO TRUE.
           MOVE 'S'                    TO DELINVCE-RETURN-STATUS-CODE.
           MOVE  1                     TO DELINVCE-RETURN-MSG-COUNT.
           MOVE SPACES                 TO DELINVCE-RETURN-MSG-NUM.
           MOVE SPACES                 TO DELINVCE-RETURN-MSG-FIELD.
           MOVE WSLG-SUCCESSFUL-MSG-TEXT
                                       TO DELINVCE-RETURN-MSG-TEXT.

           GO TO Z9999-RETURN-TO-CALLING-PGM.


       Z9999-RETURN-TO-CALLING-PGM.
      ******************************************************************
      *    RETURN TO THE CALLING PROGRAM.
      ******************************************************************

           MOVE DELINVCE-PARM-AREA     TO DFHCOMMAREA (1:LENGTH OF
                DELINVCE-PARM-AREA).

           EXEC CICS RETURN
           END-EXEC.
